# Globe Web Services

## GWS Server

### How to build

- Update versions and produce all embedded gws-services (ex : sonar-netcdf-converter version in gws-services/build.gradle)
- Update GWS version in gws/build.gradle
- Open a terminal in the root folder of the project
- Use Gradle to assemble and test the project : gradlew build
- The resulting executable jar file is ./build/libs/gws-[VERSION].jar

### How to execute

To launch GWS Server :

- you can use gradle in a terminal : gradlew bootRun
- or directly using OpenJdk : java -jar ./build/libs/gws-[VERSION].jar

To add some python services :

- Set the CONDA_PATH environment variable to the home directory of Conda or Miniconda
- Complete the launch command line with the name of the conda environment (or the full path). GWS will retrieve the
  services by running the "gws" module.

### How to deploy

Run gradle task `gws:publish`

## GWS API

'gws-api' contains several projects in different languages (Java, Python, Typescript..) which enable to interact with
GWS Server.

This subproject handles flatbuffers schemas, used to generate flatbuffers classes in all languages.

### How to build & publish

#### Configure repository (if not already done)

Before publishing, the gitlab repository and a valid TOKEN must be defined in configuration files.
_(for example : use a private token generated from gitlab personal web
page : https://gitlab.ifremer.fr/-/profile/personal_access_tokens)_

NPM configuration file `~/.npmrc` :

```
@scope:registry=https://gitlab.ifremer.fr/api/v4/projects/3478/packages/npm/
//gitlab.ifremer.fr/api/v4/projects/3478/packages/npm/:_authToken="${NPM_TOKEN}"
```

Pypi configuration file `~/.pypirc`:

```
[distutils]
index-servers =
gitlab-pygws
[gitlab-pygws]
repository = https://gitlab.ifremer.fr/api/v4/projects/3478/packages/pypi
username = <your_personal_access_token_name> 
password = <your_personal_access_token>
```

Gradle configuration file `~/.gradle/gradle.properties` (use by maven publish task):

```
gitLabPrivateToken=${NPM_TOKEN}
```

#### Typescript package : install dependencies

To build typescript package of gws-api, first run `npm install` under `../typescript` to get required discrepancies.

#### Build & publish packages

Run gradle task `gws-api:publish`, which does the following steps :

- Generate flatbuffers classes in all languages (with flatc)
- Builds and publishes the pypi package (Python) : **pygws**
- Builds and publishes the node package (Typescript) : **@feet/gws-client**
- Builds and publishes the maven package (Java) : **fr/ifremer/fleet/gws-api**

This task produces and publishes 3 packages (pypi, npm & maven) on GitLab GWS package registry.

Note : an available 'conda' & 'pyat_dev' environment are necessary (used to run : flatc, pyhon build, twine...)

### How to use

#### Python

Install pypi
package : `pip install pygws --index-url https://gitlab.ifremer.fr/api/v4/projects/3478/packages/pypi/simple`

#### TypeScript

Install NPM package : `npm i @fleet/gws-client`

With registry setup in .npmrc file : `@fleet:registry=https://gitlab.ifremer.fr/api/v4/packages/npm/`

#### Java

Add maven dependency :

```
    <repository>
        <id>gitlab-gws</id>
        <url>https://gitlab.ifremer.fr/api/v4/projects/3478/packages/maven</url>
    </repository>
    ...
    <dependency>
      <groupId>fr.ifremer.fleet</groupId>
      <artifactId>gws-api</artifactId>
      <version>0.1.0</version>
    </dependency>
```
