-- Drop table on server start to ensure no service is present
-- Tables are automatically recreated by hibernate
DROP TABLE IF EXISTS GWS_SERVICE;
DROP TABLE IF EXISTS GWS_SERVICE_GROUP;