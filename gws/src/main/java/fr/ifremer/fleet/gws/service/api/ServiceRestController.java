/**
 *
 */
package fr.ifremer.fleet.gws.service.api;

import com.fasterxml.jackson.databind.JsonNode;
import fr.ifremer.fleet.gws.batch.dto.JobDto;
import fr.ifremer.fleet.gws.service.controller.ServiceManager;
import fr.ifremer.fleet.gws.service.domain.GwsService;
import fr.ifremer.fleet.gws.service.domain.GwsServiceGroup;
import fr.ifremer.fleet.gws.service.dto.ServiceDto;
import fr.ifremer.fleet.gws.service.dto.ServiceGroupDto;
import fr.ifremer.fleet.gws.service.repository.ServiceGroupRepository;
import fr.ifremer.fleet.gws.service.repository.ServiceRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.JobExecutionException;
import org.springframework.batch.core.JobParameters;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Controller the Rest API for services.
 */
@RestController
@RequestMapping("/gws/services")
@Slf4j
public class ServiceRestController {

    /**
     * Repository of groups
     */
    private final ServiceGroupRepository groupRepo;
    /**
     * Repository of services
     */
    private final ServiceRepository serviceRepo;

    private final ServiceManager serviceController;

    /**
     * Constructor with repository injection
     */
    ServiceRestController(ServiceGroupRepository groupRepo, ServiceRepository repository, ServiceManager serviceController) {
        this.groupRepo = groupRepo;
        this.serviceRepo = repository;
        this.serviceController = serviceController;
    }

    /**
     * Return the JSon of a detailed GwsService (with the content of the description
     * and help files)
     */
    @GetMapping("/{id}")
    public ResponseEntity<ServiceDto> getServiceDetails(@PathVariable("id") Long id) {
        return serviceRepo.findById(id)//
                .map(ServiceDto::mapDetailed)//
                .map(ResponseEntity::ok)//
                .orElse(ResponseEntity.notFound()//
                        .build());
    }

    /**
     * Return the JSon of all GwsService present in the repository
     * Depending on the parameter "format", the result is a plain list of service (format=list) or tree (the default, format=tree)
     */
    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    Object listAllServices(@RequestParam(required = false, defaultValue = "tree") String format) {
        if ("list".equalsIgnoreCase(format))
            return serviceRepo.findAll().stream().map(ServiceDto::map).toList();

        // Tree format
        GwsServiceGroup rootGroup = groupRepo.getRootGroup();
        return ServiceGroupDto.map(rootGroup);
    }

    /**
     * Launch a service
     */
    @PostMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<JobDto> launchService(@PathVariable("id") Long id, @RequestParam(required = false, defaultValue = "false") boolean logbook, @RequestBody JsonNode parameters) {
        try {
            GwsService service = serviceRepo.findById(id).orElse(null);
            if (service != null) {
                try {
                    JobParameters JobParameters = serviceController.launch(service, parameters, logbook);
                    return ResponseEntity.ok(JobDto.map(JobParameters));
                } catch (JobExecutionException e) {
                    log.warn("Can not execute service " + service.getName(), e);
                    return ResponseEntity.unprocessableEntity().build();
                }
            } else
                return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } catch (Exception e) {
            // Occurred when too many jobs are already running.
            return ResponseEntity.status(HttpStatus.TOO_MANY_REQUESTS).build();
        }
    }
}
