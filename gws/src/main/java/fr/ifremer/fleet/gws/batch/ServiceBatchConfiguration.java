package fr.ifremer.fleet.gws.batch;

import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.support.TaskExecutorJobLauncher;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.boot.autoconfigure.batch.BatchDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import javax.sql.DataSource;

/**
 * Configuration of Spring batch
 */
@Configuration
@EnableBatchProcessing(dataSourceRef = "batchDataSource")
@Slf4j
public class ServiceBatchConfiguration {

    /**
     * Configure a H2 base to store the jobs
     */
    @Bean
    @BatchDataSource
    DataSource batchDataSource() {
        return new EmbeddedDatabaseBuilder()//
                .setName("batch-db")//
                .setType(EmbeddedDatabaseType.H2)//
                .addScript("/org/springframework/batch/core/schema-h2.sql")//
                .build();
    }

    /**
     * Configure a JobLauncher to launch jobs asynchronously
     */
    @Bean
    JobLauncher jobLauncher(JobRepository jobRepository) throws Exception {
        TaskExecutorJobLauncher jobLauncher = new TaskExecutorJobLauncher();
        jobLauncher.setJobRepository(jobRepository);
        jobLauncher.afterPropertiesSet();

        // Allow 100 running services simultaneously
        var taskExecutor = new SimpleAsyncTaskExecutor();
        taskExecutor.setConcurrencyLimit(100);
        jobLauncher.setTaskExecutor(taskExecutor);

        return jobLauncher;
    }

}
