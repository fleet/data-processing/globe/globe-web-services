package fr.ifremer.fleet.gws.logbook.domain;

import fr.ifremer.fleet.gws.logbook.dto.LogBookEventDto;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Inheritance;
import jakarta.persistence.InheritanceType;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.time.LocalDateTime;

/**
 * Abstract LogBook event
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Data
@SuperBuilder
@NoArgsConstructor
public abstract class GwsLogBookEvent {

    /**
     * Unique id of the event
     */
    @Id
    private Long id;

    /**
     * Textual description
     */
    private String description;

    /**
     * Creation date
     */
    private LocalDateTime date;

    /**
     * Map this event to a DTO
     */
    public abstract LogBookEventDto mapToDto();

    /**
     * Map this event to a detailed DTO
     */
    public abstract LogBookEventDto mapToDetailedDto();
}
