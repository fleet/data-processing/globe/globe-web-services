package fr.ifremer.fleet.gws.service.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.PropertyNamingStrategies.SnakeCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import fr.ifremer.fleet.gws.service.domain.GwsServiceGroup;
import lombok.Builder;

import java.util.List;

/**
 * Object to transfer Service Group data
 */
@Builder
@JsonNaming(value = SnakeCaseStrategy.class)
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public record ServiceGroupDto(

        String name, // Name of the group
        List<ServiceGroupDto> subGroups, // List of the inner group
        List<ServiceDto> services //   List of service
) {

    /**
     * Map a GwsServiceGroup to its DTO
     */
    public static ServiceGroupDto map(GwsServiceGroup group) {
        return ServiceGroupDto.builder()//
                .name(group.getName())//
                .services(List.of())//
                .subGroups(group.getSubGroups() != null ? group.getSubGroups().stream().map(ServiceGroupDto::map).toList() : List.of())//
                .services(group.getServices() != null ? group.getServices().stream().map(ServiceDto::map).toList() : List.of())//
                .build();
    }

}
