package fr.ifremer.fleet.gws.batch;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.ifremer.fleet.gws.batch.monitoring.ServiceProcessMonitor;
import fr.ifremer.fleet.gws.service.domain.GwsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.SystemCommandTasklet;
import org.springframework.batch.repeat.RepeatStatus;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Tasklet of a job to run a service in a process
 */
@Slf4j
public class ProcessTasklet extends SystemCommandTasklet {

    /**
     * Service to execute
     */
    private final GwsService service;
    /**
     * Parameters of service to execute
     */
    private final JsonNode serviceParameters;

    /**
     * Json temporary file containing all arguments
     */
    private final File argumentTempFile;

    private final ServiceProcessMonitor processMonitor;
    /**
     * Extracted jar to delete after execution
     */
    private final Optional<File> tempJarFile = Optional.empty();

    /**
     * Constructor
     */
    public ProcessTasklet(GwsService service, JsonNode serviceParameters, ServiceProcessMonitor processMonitor) throws IOException {
        this.service = service;
        this.serviceParameters = serviceParameters;
        this.processMonitor = processMonitor;
        argumentTempFile = File.createTempFile("gws_service_param", ".json");

        setCommandRunner(this::launchProcess);
        setSystemProcessExitCodeMapper(exitCode -> exitCode == 0 ? ExitStatus.COMPLETED : new ExitStatus(ExitStatus.FAILED.getExitCode(), "Process failed, return code is " + exitCode));
    }

    /**
     * Specify how to launch the process
     */
    private Process launchProcess(String[] command, String[] envp, File dir) throws IOException {
        ProcessBuilder builder = new ProcessBuilder(command);
        builder.directory(dir);

        // Launch process
        Process process = builder.start();
        processMonitor.monitor(process);
        return process;
    }

    /**
     * Before executing the service, serialize all parameters in a json file
     */
    @Override
    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
        // Serialize arguments in json file
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(argumentTempFile, serviceParameters);

        // Complete command line
        List<String> command = new ArrayList(this.service.getCommand());
        command.add(argumentTempFile.getAbsolutePath());
        log.info("Running {}", String.join(" ", command));
        setCommand(command.toArray(String[]::new));

        // Launch execution
        return super.execute(contribution, chunkContext);
    }

    /**
     * Delete the JSON temporary after the execution of the service
     */
    @Override
    public ExitStatus afterStep(StepExecution stepExecution) {
        if (argumentTempFile != null && argumentTempFile.exists())
            argumentTempFile.delete();
        tempJarFile.ifPresent(File::delete);
        return super.afterStep(stepExecution);
    }
}
