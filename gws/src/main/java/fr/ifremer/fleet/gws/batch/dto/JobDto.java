/**
 *
 */
package fr.ifremer.fleet.gws.batch.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.PropertyNamingStrategies.SnakeCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import fr.ifremer.fleet.gws.service.controller.ServiceManager;
import lombok.Builder;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;

import java.time.LocalDateTime;

/**
 * Object to transfer Service data
 */
@Builder
@JsonNaming(value = SnakeCaseStrategy.class)
@JsonInclude(value = Include.NON_NULL)
public record JobDto(
        Long jobId, // id of the job
        Long serviceId, // id of a service
        String serviceName,
        String websocketHost, // RSocket address
        Long websocketPort, // RSocket port
        JobStatus status,
        LocalDateTime createTime, // When job was submitted
        LocalDateTime startTime, // When job started running
        LocalDateTime endTime // When job finished
) {

    /**
     * Map a JobExecution to its DTO
     */
    public static JobDto map(JobParameters jobParameters) {
        return JobDto.builder()//
                .jobId(null)//
                .serviceId(jobParameters.getLong(ServiceManager.JOB_PARAM_SERVICE_ID))//
                .serviceName(jobParameters.getString(ServiceManager.JOB_PARAM_SERVICE_NAME))//
                .websocketHost(jobParameters.getString(ServiceManager.JOB_PARAM_WEBSOCKET_HOST))//
                .websocketPort(jobParameters.getLong(ServiceManager.JOB_PARAM_WEBSOCKET_PORT))//
                .status(JobStatus.PENDING)//
                .build();
    }

    /**
     * Map a JobExecution to its DTO
     */
    public static JobDto map(JobExecution jobExecution) {
        return JobDto.builder()//
                .jobId(jobExecution.getId())//
                .serviceId(jobExecution.getJobParameters().getLong(ServiceManager.JOB_PARAM_SERVICE_ID))//
                .serviceName(jobExecution.getJobParameters().getString(ServiceManager.JOB_PARAM_SERVICE_NAME))//
                .websocketHost(jobExecution.getJobParameters().getString(ServiceManager.JOB_PARAM_WEBSOCKET_HOST))//
                .websocketPort(jobExecution.getJobParameters().getLong(ServiceManager.JOB_PARAM_WEBSOCKET_PORT))//
                .status(JobStatus.compute(jobExecution))//
                .build();
    }

    /**
     * Map a JobExecution to its detailed DTO
     */
    public static JobDto mapDetailed(JobExecution jobExecution) {
        return JobDto.builder()//
                .jobId(jobExecution.getId())//
                .serviceId(jobExecution.getJobParameters().getLong(ServiceManager.JOB_PARAM_SERVICE_ID))//
                .serviceName(jobExecution.getJobParameters().getString(ServiceManager.JOB_PARAM_SERVICE_NAME))//
                .websocketHost(jobExecution.getJobParameters().getString(ServiceManager.JOB_PARAM_WEBSOCKET_HOST))//
                .websocketPort(jobExecution.getJobParameters().getLong(ServiceManager.JOB_PARAM_WEBSOCKET_PORT))//
                .createTime(jobExecution.getCreateTime())//
                .startTime(jobExecution.getStartTime())//
                .endTime(jobExecution.getEndTime())//
                .status(JobStatus.compute(jobExecution))//
                .build();
    }
}
