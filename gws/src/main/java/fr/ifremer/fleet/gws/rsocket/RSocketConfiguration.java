/**
 *
 */
package fr.ifremer.fleet.gws.rsocket;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.codec.cbor.Jackson2CborDecoder;
import org.springframework.http.codec.cbor.Jackson2CborEncoder;
import org.springframework.messaging.rsocket.RSocketStrategies;
import org.springframework.messaging.rsocket.annotation.support.RSocketMessageHandler;

/**
 * Configure a RSocketMessageHandler for handling RSocket requests
 */
@Configuration
public class RSocketConfiguration {

    @Bean
    RSocketMessageHandler rsocketMessageHandler() {
        RSocketMessageHandler handler = new RSocketMessageHandler();
        handler.setRSocketStrategies(rsocketStrategies());
        return handler;
    }

    RSocketStrategies rsocketStrategies() {
        return RSocketStrategies.builder()//
                .encoders(encoders -> encoders.add(new Jackson2CborEncoder()))//
                .decoders(decoders -> decoders.add(new Jackson2CborDecoder()))//
                // .routeMatcher(new PathPatternRouteMatcher())//
                .build();
    }
}
