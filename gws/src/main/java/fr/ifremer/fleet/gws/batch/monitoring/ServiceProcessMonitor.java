package fr.ifremer.fleet.gws.batch.monitoring;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.ifremer.fleet.gws.ApplicationContextProvider;
import fr.ifremer.fleet.gws.batch.dto.JobStatus;
import fr.ifremer.fleet.gws.logbook.controller.LogBookController;
import fr.ifremer.fleet.gws.logbook.domain.GwsJobEvent;
import fr.ifremer.fleet.gws.rsocket.GwsPayload;
import fr.ifremer.fleet.gws.rsocket.PayloadInterceptor;
import fr.ifremer.fleet.gws.rsocket.TwinnedRSocketServers;
import fr.ifremer.fleet.gws.rsocket.flatbuffers.LogLevel;
import fr.ifremer.fleet.gws.rsocket.flatbuffers.LogPayload;
import fr.ifremer.fleet.gws.service.domain.GwsService;
import io.rsocket.Payload;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.exec.PumpStreamHandler;
import org.apache.commons.lang3.StringUtils;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Arrays;

/**
 * This class aim monitor the execution of a service. Reacts when process of service starts and stops
 */
@Slf4j
public class ServiceProcessMonitor implements JobExecutionListener, PayloadInterceptor {

    /**
     * Service to execute
     */
    private final GwsService service;

    /**
     * Parameters of service to execute
     */
    private final JsonNode serviceParameters;

    /**
     * Handler of output stream of the process
     */
    private final ServiceLogOutputStream serviceOutStream;
    /**
     * Handler of error stream of the process
     */
    private final ServiceLogOutputStream serviceErrStream;
    /**
     * PumpStreamHandler catching the output stream of the process
     */
    private final PumpStreamHandler streams;

    /**
     * True when a JobEvent is created when service starts
     */
    private final boolean recordToLogBook;


    private final TwinnedRSocketServers rsocketServers;

    /**
     * Job event created when process to monitor is set
     */
    private GwsJobEvent jobEvent;

    /**
     * Log file when all intercepted log message are written
     */
    private PrintStream logFileStream;

    /**
     * True when the terminate signal passed
     */
    private boolean terminateReceived;

    public ServiceProcessMonitor(GwsService service, JsonNode serviceParameters, TwinnedRSocketServers rsocketServers, boolean recordToLogBook) throws IOException {
        this.service = service;
        this.serviceParameters = serviceParameters;
        this.recordToLogBook = recordToLogBook;
        this.rsocketServers = rsocketServers;

        this.serviceOutStream = new ServiceLogOutputStream(LogLevel.INFO, rsocketServers);
        this.serviceErrStream = new ServiceLogOutputStream(LogLevel.ERROR, rsocketServers);
        this.streams = new PumpStreamHandler(serviceOutStream, serviceErrStream);

        rsocketServers.getClientServer().setPayloadInterceptor(this);
    }

    /**
     * Start monitoring the process
     */
    public void monitor(Process process) {
        // Manage redirection of console logs
        streams.setProcessInputStream(process.getOutputStream());
        streams.setProcessOutputStream(process.getInputStream());
        streams.setProcessErrorStream(process.getErrorStream());
        streams.start();

        // Kill the process when the client disconnects
        rsocketServers.getClientServer().onClose().doFinally(c -> process.destroy()).subscribe();

        // Process stopped
        process.onExit().thenApply(p -> {
            onProcessExit();
            return process;
        });
    }

    public void beforeJob(JobExecution jobExecution) {

        if (recordToLogBook) {
            LogBookController logBookController = ApplicationContextProvider.getInContext(LogBookController.class);
            jobEvent = logBookController.addGwsJobEvent(service, serviceParameters);
            initializeLogFile();
        }

        // Log command and parameters
        String commandToLog = "Command : " + String.join(" ", service.getCommand());
        rsocketServers.getClientServer()
                .sendToEmitterSocket(GwsPayload.makeLogPayload(LogLevel.INFO, commandToLog, "", "").makePayload())
                .block();
        try {
            ObjectMapper mapper = new ObjectMapper();
            String parametersToLog = "Parameters : \n" + mapper.writerWithDefaultPrettyPrinter().writeValueAsString(serviceParameters);
            rsocketServers.getClientServer()
                    .sendToEmitterSocket(GwsPayload.makeLogPayload(LogLevel.INFO, parametersToLog, "", "").makePayload())
                    .block();
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Spring batch alerts that the job stopped.
     * At this point, process has already exited.
     */
    public void afterJob(JobExecution jobExecution) {

        // Log an error if job failed
        if (jobExecution.getExitStatus().getExitCode().equals(ExitStatus.FAILED.getExitCode())) {
            String message = jobExecution.getExitStatus().getExitDescription();
            rsocketServers.getClientServer()
                    .sendToEmitterSocket(GwsPayload.makeLogPayload(LogLevel.ERROR, !message.isEmpty() ? message : "Service failed", "", "").makePayload())
                    .onErrorComplete() // Connection may be closed
                    .block();
        }

        // Send a terminate signal if not already done
        if (!terminateReceived)
            rsocketServers.getClientServer()
                    .sendToEmitterSocket(GwsPayload.makeTerminatePayload().makePayload())
                    .onErrorComplete() // Client has already close the socket
                    .block();


        // Finish the job event
        if (jobEvent != null) {
            LogBookController logBookController = ApplicationContextProvider.getInContext(LogBookController.class);
            logBookController.closeJobEvent(jobEvent, jobExecution.getStatus() == BatchStatus.COMPLETED ? JobStatus.SUCCESS : JobStatus.ERROR);
            logFileStream.close();
        }

        onProcessExit();
    }

    /**
     * Called when process exit.
     */
    private void onProcessExit() {
        // Close console streams
        try {
            serviceOutStream.close();
            serviceErrStream.close();
            streams.stop();
        } catch (IOException e) {
            log.warn(e.getMessage());
        }
    }

    private void initializeLogFile() {
        if (jobEvent != null) {
            PrintStream logStream = null;
            File logFile = jobEvent.getLogFile();
            logFile.getParentFile().mkdirs();
            try {
                logStream = new PrintStream(logFile);
            } catch (FileNotFoundException e) {
                log.error("Unable to redirect process output to file {} : {}", logFile.getPath(), e.getMessage());
            }
            this.logFileStream = logStream;
        }
    }

    /**
     * Intercepts payload.
     * Prefix the log message with the job event id.
     * Keep in memory a terminate signal
     */
    @Override
    public Payload apply(Payload payload) {
        // Intercepts payload only if jobEvent is set
        if (jobEvent == null)
            return payload;

        GwsPayload gwsPayload = GwsPayload.from(payload);
        if (GwsPayload.TERMINATE_ROUTE.equals(gwsPayload.route()))
            this.terminateReceived = true;
        // Intercepts Logs
        if (!GwsPayload.LOG_ROUTE.equals(gwsPayload.route()))
            return payload;

        LogPayload originalLog = LogPayload.getRootAsLogPayload(gwsPayload.payload());
        String message = originalLog.message();
        String date = originalLog.date();
        String stack = originalLog.stack();
        int logLevel = originalLog.level();

        // Keep first message error
        if (logLevel == LogLevel.ERROR && jobEvent.getError() == null)
            jobEvent.setError(StringUtils.abbreviate(message, 255));

        // Write message in log file
        logFileStream.println(date + "|" + LogLevel.name(logLevel) + "| " + message);
        if (stack != null && !stack.isEmpty())
            Arrays.stream(stack.split("\n")).forEach(logFileStream::println);

        // Return a LogPayload prefixed with the job event id
        return GwsPayload.makeLogPayload(logLevel, "#" + jobEvent.getId() + " " + message, date, stack).makePayload();
    }

    public TwinnedRSocketServers getRsocketServers() {
        return rsocketServers;
    }

}
