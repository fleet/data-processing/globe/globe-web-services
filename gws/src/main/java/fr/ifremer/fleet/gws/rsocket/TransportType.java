package fr.ifremer.fleet.gws.rsocket;

/**
 * Type of socket used to bind a TwinRSocket
 */
public enum TransportType {
    TCP, WEB_SOCKET
}
