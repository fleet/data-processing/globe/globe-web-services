package fr.ifremer.fleet.gws.service.controller;

import fr.ifremer.fleet.gws.rsocket.TwinnedRSocketServers;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.stream.IntStream;

/**
 * Pool of PooledProcess.
 * These PooledProcess were prepared
 */
@Slf4j
public class ProcessPool {

    /**
     * Command line used to run one process
     */
    private final String[] command;

    /**
     * All processes currently running and waiting for a service request
     */
    private final List<PooledProcess> pooledProcesses = new LinkedList<>();

    /**
     * Constructor.
     *
     * @param command     Command line used to run one process. This is also the id of the pool (hascode)
     * @param initialSize number of expected process in the pool
     */
    public ProcessPool(String[] command, int initialSize) {
        this.command = command;
        addPooledProcess(initialSize);
    }

    /**
     * Get one of the PooledProcess from the pool.
     * Can return Optional.empty() when pool is empty, and it is not possible to create a new one.
     */
    public Optional<PooledProcess> getPooledService() {
        synchronized (pooledProcesses) {
            if (!pooledProcesses.isEmpty()) {
                // Get the first process from the pool.
                PooledProcess result = pooledProcesses.remove(0);
                // Prepare a new one.
                addPooledProcess(1);

                return Optional.of(result);
            }
        }

        // No process available. Try to run a new one
        return createPooledProcess();
    }

    /**
     * Creates some PooledProcess and add them to the pool
     */
    private void addPooledProcess(int processCount) {
        IntStream.range(0, processCount)
                .mapToObj(i -> createPooledProcess())
                .filter(Optional::isPresent)
                .map(Optional::get)
                .forEach(pooledProcesses::add);
    }

    /**
     * Set up a PooledProcess by running the command line in a process and opening RSocket servers
     */
    private Optional<PooledProcess> createPooledProcess() {
        try {
            // Launch the RSocket bridge
            TwinnedRSocketServers servers = new TwinnedRSocketServers();
            log.info("Preparing a TwinnedRSocketServers with port {} and tcp port {}", servers.getWebsocketPort(), servers.getTcpPort());

            // Prepare the command line to launch the Pooled service.
            List<String> command = new ArrayList(List.of(this.command));
            command.add(String.valueOf(servers.getTcpPort()));
            log.info("Running {}", String.join(" ", command.toArray(String[]::new)));


            // Run the command and get the process
            ProcessBuilder builder = new ProcessBuilder(command);
            builder.directory(new File(System.getProperty("user.dir")));
            Process process = builder.start();

            return Optional.of(new PooledProcess(servers, process));
        } catch (IOException e) {
            log.warn("Unable to create a new process for the pool", e);
            return Optional.empty();
        }
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProcessPool that = (ProcessPool) o;
        return Arrays.equals(command, that.command);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(command);
    }
}
