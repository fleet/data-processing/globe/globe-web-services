package fr.ifremer.fleet.gws.logbook.domain;

public record GwsEnvVar(String variable, String value) {
}
