package fr.ifremer.fleet.gws.service.repository;

import fr.ifremer.fleet.gws.service.domain.GwsService;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * Repository of GwsService, automatically generates a CRUD for services
 */
public interface ServiceRepository extends JpaRepository<GwsService, Long> {
    /**
     * Return the list of service with the specified path
     */
    Optional<GwsService> findByGroupPathAndName(String groupPath, String name);

}
