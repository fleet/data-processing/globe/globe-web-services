package fr.ifremer.fleet.gws.service.loader;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategies.SnakeCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import fr.ifremer.fleet.gws.service.controller.ServiceManager;
import fr.ifremer.fleet.gws.service.domain.GwsService;
import fr.ifremer.fleet.gws.service.domain.GwsServiceGroup;
import fr.ifremer.fleet.gws.service.repository.ServiceGroupRepository;
import fr.ifremer.fleet.gws.service.repository.ServiceRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

/**
 * Loader of Python services.<br>
 * Creates instances of GwsService for python service and add them to the repository
 */
@Configuration // This class has of some @Bean methods
@Slf4j // Generates a log instance
public class PythonServiceLoader {

    private final ServiceGroupRepository groupRepo;
    private final ServiceRepository serviceRepo;
    private final ServiceManager serviceManager;

    @Autowired
    public PythonServiceLoader(ServiceGroupRepository groupRepo, ServiceRepository serviceRepo, ServiceManager serviceManager) {
        this.groupRepo = groupRepo;
        this.serviceRepo = serviceRepo;
        this.serviceManager = serviceManager;
    }

    /**
     * Annotation Bean : Produce some beans to manage in Spring container<br>
     * Return a ApplicationRunner to trigger when application is starting
     *
     * @return a ApplicationRunner (Bean) to run
     */
    @Bean
    ApplicationRunner parseServiceFiles() {
        return args /* ApplicationArguments */ -> {
            List<String> condaEnvs = args.getNonOptionArgs();
            if (!condaEnvs.isEmpty()) {

                long serviceCount = 0;
                for (String condaEnv : condaEnvs) {
                    File tempConfFile = null;
                    try {
                        log.info("Loading service configuration for Conda environment {} ", condaEnv);
                        tempConfFile = File.createTempFile("Python_Configuration_", ".json");
                        if (readConfiguration(condaEnv, tempConfFile) && tempConfFile.isFile()) {
                            loadConfigurationFile(condaEnv, tempConfFile);
                            serviceRepo.flush();
                            log.info("Nb of loaded service from Conda environment {} : {}", condaEnv,
                                    serviceRepo.count() - serviceCount);
                            serviceCount = serviceRepo.count();
                        }
                    } catch (Exception e) {
                        log.error("Error while loading service configuration for Conda environment {} : {} ", condaEnv, e.getMessage());
                    } finally {
                        if (tempConfFile != null)
                            tempConfFile.delete();
                    }
                }
            } else
                log.warn("No Conda environment provided in command line. No python service loaded");
        };
    }

    /**
     * Execute the python module "gws" to get the service configuration
     */
    private boolean readConfiguration(String condaEnv, File outConfFile) {
        ProcessBuilder builder = new ProcessBuilder(formatCommand(condaEnv, "gws"));
        builder.redirectOutput(outConfFile);
        builder.redirectError();
        try {
            int rc = builder.start().waitFor();
            if (rc == 0)
                return true;
            log.error("Getting service configuration from Conda environment {} failed, return code is {}", condaEnv, rc);
            try {
                log.warn(Files.readString(outConfFile.toPath()));
            } catch (IOException e) {
                log.debug("Cannot read the configuration file : {}", e.getMessage());
            }
        } catch (Exception e) {
            log.error("Getting service configuration from Conda environment '{}' failed : {}", condaEnv, e.getMessage());
        }
        return false;
    }

    /**
     * Parse one the specified configuration file. Create the ServiceGroup and all
     * services
     */
    private void loadConfigurationFile(String condaEnv, File confFile) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        GwsConfigurationFile[] gwsConfigurations = mapper.readValue(confFile, GwsConfigurationFile[].class);

        for (GwsConfigurationFile gwsConfiguration : gwsConfigurations) {
            String pythonModule = gwsConfiguration.pythonModule();
            String[] commandLine = formatCommand(condaEnv, pythonModule);

            // Set up a pool of process if required by the configuration
            if (gwsConfiguration.processPoolSize != null)
                serviceManager.initializeServicePool(commandLine, gwsConfiguration.processPoolSize.intValue());

            // Parse the configuration file and load services
            GwsServiceGroup rootGroup = groupRepo.getRootGroup();
            JsonNode jsonNode = gwsConfiguration.services();
            ParsingParameters parsingParameters = new ParsingParameters(rootGroup, jsonNode, gwsConfiguration, List.of(commandLine));
            parseNode(parsingParameters);
        }
    }

    /**
     * Manually parse a JsonNode. Extracts subgroups and services
     */
    private void parseNode(ParsingParameters params) {
        Iterator<Entry<String, JsonNode>> fields = params.jsonNode.fields();
        while (fields.hasNext()) {
            Entry<String, JsonNode> entry = fields.next();
            switch (entry.getKey()) {
                case "apps": // Services of the current service group
                    parseServices(params.withJsonNode(params.serviceGroup, entry.getValue()));
                    break;
                case "children": // Subgroups of the current service group
                    parseNode(params.withJsonNode(params.serviceGroup, entry.getValue()));
                    break;
                default: // Name of a subgroup
                    GwsServiceGroup subGroup = groupRepo.findOrCreateSubGroupByName(params.serviceGroup, entry.getKey());
                    parseNode(params.withJsonNode(subGroup, entry.getValue()));
            }
        }
    }

    /**
     * Browse all configuration file of service. Parse them and extract services
     */
    private void parseServices(ParsingParameters params) {
        // Defining the root folder of the configuration files
        File rootConfigurationFolder = new File(params.gwsConfiguration().confRootFolder());
        // Defining the root folder of the help files
        File rootHelpFolder = new File(params.gwsConfiguration().helpRootFolder());

        Iterator<JsonNode> serviceIter = params.jsonNode.elements();
        while (serviceIter.hasNext()) {
            var service = new GwsService();
            File descriptionFile = new File(rootConfigurationFolder, serviceIter.next().asText());
            if (descriptionFile.exists()) {
                try {
                    service.setDescriptionPath(descriptionFile.getCanonicalPath());
                } catch (IOException e) {
                    service.setDescriptionPath(descriptionFile.getAbsolutePath());
                }
                service.setParentGroup(params.serviceGroup);
                service.setGroupPath(params.serviceGroup.getPath());

                // Parsing the configuration file to get the name, bootstrap and the help
                try {
                    ObjectMapper mapper = new ObjectMapper();
                    ServiceDesc serviceDesc = mapper.readValue(descriptionFile, ServiceDesc.class);

                    // Name
                    if (serviceDesc.name() != null && !serviceDesc.name().isEmpty()) {
                        service.setName(serviceDesc.name());
                    } else {
                        log.warn("Service '{}' has no name defined. ", service.getDescriptionPath());
                        service.setName("");
                    }

                    // Help
                    if (serviceDesc.help() != null && !serviceDesc.help().isEmpty()) {
                        service.setHelpPath(new File(rootHelpFolder, serviceDesc.help()).getAbsolutePath());
                    } else {
                        log.warn("Service '{}' has no help file", service.getDescriptionPath());
                        service.setHelpPath("");
                    }

                    // Which python module to run for executing the service
                    service.setCommand(params.command);

                    serviceRepo.save(service);
                } catch (Exception e) {
                    log.warn("Error when parsing configuration file. Service '{}' discarded",
                            service.getDescriptionPath());
                }
            } else
                log.warn("Configuration file of service not found ({}). Ignored", descriptionFile.getPath());
        }
    }

    /**
     * Format the command line to run the specified python module in the conda environment with a ProcessBuilder
     */
    private String[] formatCommand(String condaEnv, String pythonModule) {
        String condaExec = System.getenv("CONDA_PATH");
        String os = System.getProperty("os.name").toLowerCase();
        if (os.contains("win"))
            condaExec += "\\condabin\\conda.bat";
        else
            condaExec += "/condabin/conda";

        // Using full path to environment location (i.e. prefix).
        if (new File(condaEnv).isDirectory())
            return new String[]{condaExec, "run", "-p", condaEnv, "python", "-m", pythonModule};

        return new String[]{condaExec, "run", "-n", condaEnv, "python", "-m", pythonModule};
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    private record ServiceDesc(String name, String file, String help) {
    }

    @JsonNaming(value = SnakeCaseStrategy.class)
    private record GwsConfigurationFile(
            String helpRootFolder, String confRootFolder, String pythonModule, Integer processPoolSize,
            JsonNode services) {
    }

    private record ParsingParameters(GwsServiceGroup serviceGroup, JsonNode jsonNode,
                                     GwsConfigurationFile gwsConfiguration,
                                     // Command line to use for running the service
                                     List<String> command) {
        public ParsingParameters withJsonNode(GwsServiceGroup serviceGroup, JsonNode jsonNode) {
            return new ParsingParameters(serviceGroup, jsonNode, gwsConfiguration, command);
        }
    }

}
