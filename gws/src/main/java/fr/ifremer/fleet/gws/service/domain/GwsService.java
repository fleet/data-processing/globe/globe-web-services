package fr.ifremer.fleet.gws.service.domain;

import jakarta.persistence.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * Plain service
 */
@Entity // Make a service ready for storage in a JPA-based data store
@Data // Generates getters, setters, equals, hashcode...
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class GwsService {

    /**
     * Unique id of a service
     */
    private @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @EqualsAndHashCode.Include Long id;

    /**
     * Name
     */
    private String name;

    /**
     * Complete path of the service (/root/groups/...)
     */
    private String groupPath;

    /**
     * Json description file. <br>
     * Can be a file path or a URL (starting with file: or jar:)
     * This file contains the description of all arguments expected by the service
     */
    @Column(length = 1024)
    private String descriptionPath;

    /**
     * String list containing the program and its arguments for executing a service with a ProcessBuilder
     */
    private List<String> command;

    /**
     * Help file in HTML format<br>
     * Can be a file path or a URL (starting with file: or jar:)
     */
    @Column(length = 1024)
    private String helpPath;

    /**
     * Link to parent group
     */
    @ManyToOne(fetch = FetchType.LAZY)
    private GwsServiceGroup parentGroup;

}
