package fr.ifremer.fleet.gws.rsocket;

import io.rsocket.Payload;

import java.util.function.Function;

/**
 * Instance of interceptor used to patch an emitted payload
 */
public interface PayloadInterceptor extends Function<Payload, Payload> {
    /**
     * Inert interceptor .
     */
    static PayloadInterceptor identity() {
        return payload -> payload;
    }
}
