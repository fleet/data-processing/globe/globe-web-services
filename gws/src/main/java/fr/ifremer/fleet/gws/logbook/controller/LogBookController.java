package fr.ifremer.fleet.gws.logbook.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.ifremer.fleet.gws.batch.dto.JobStatus;
import fr.ifremer.fleet.gws.logbook.domain.GwsJobEvent;
import fr.ifremer.fleet.gws.logbook.repository.LogBookEventRepository;
import fr.ifremer.fleet.gws.service.domain.GwsService;
import fr.ifremer.fleet.gws.service.repository.ServiceGroupRepository;
import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.time.LocalDateTime;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Controller of logbook event.
 */
@Slf4j
@Controller
public class LogBookController {

    /**
     * Using atomic instead of db sequence (to prevent hibernate from allocating a pool of ids and thus having holes in the ids each time the server is restarted)
     */
    private static final AtomicLong currentId = new AtomicLong(0L);
    @Autowired
    private LogBookEventRepository logBookEventRepository;
    @Autowired
    private ServiceGroupRepository serviceGroupRepository;

    @PostConstruct
    private void initialize() {
        Long maxId = logBookEventRepository.getMaxId();

        // null when table is empty
        if (maxId != null) {
            currentId.set(maxId.longValue());
            // When server starts, no job is supposed to run
            logBookEventRepository.cancelAllRunning();
        }
    }

    /**
     * Asks the controller to create a new job event
     */
    public GwsJobEvent addGwsJobEvent(GwsService service, JsonNode serviceParameters) {
        String parameters = "";
        try {
            parameters = new ObjectMapper().writeValueAsString(serviceParameters);
        } catch (JsonProcessingException e) {
            log.warn("Service parameters not serializable");
        }

        GwsJobEvent result = GwsJobEvent.builder()
                .id(currentId.incrementAndGet())
                .description(service.getName())
                .date(LocalDateTime.now())
                .groupPath(service.getGroupPath())
                .status(JobStatus.RUNNING)
                .parameters(parameters)
                .build();
        return logBookEventRepository.saveAndFlush(result);
    }

    /**
     * Asks the controller to finalize a job event
     */
    public void closeJobEvent(GwsJobEvent event, JobStatus status) {
        event.setStatus(event.getError() == null ? status : JobStatus.ERROR);
        event.setEndDate(LocalDateTime.now());
        logBookEventRepository.saveAndFlush(event);
    }


}
