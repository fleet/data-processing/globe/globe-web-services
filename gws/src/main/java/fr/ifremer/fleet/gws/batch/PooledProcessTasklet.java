package fr.ifremer.fleet.gws.batch;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.ifremer.fleet.gws.batch.monitoring.ServiceProcessMonitor;
import fr.ifremer.fleet.gws.rsocket.GwsPayload;
import fr.ifremer.fleet.gws.service.controller.PooledProcess;
import fr.ifremer.fleet.gws.service.domain.GwsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import java.io.IOException;

/**
 * Tasklet of a job to launch a service in a process
 */
@Slf4j
public class PooledProcessTasklet implements Tasklet {

    /**
     * Service to execute
     */
    private final PooledProcess pooledProcess;
    /**
     * Service to execute
     */
    private final GwsService service;
    /**
     * Parameters of service to execute
     */
    private final JsonNode serviceParameters;

    private final ServiceProcessMonitor processMonitor;

    /**
     * Constructor
     */
    public PooledProcessTasklet(PooledProcess pooledProcess, GwsService service, JsonNode serviceParameters, ServiceProcessMonitor processMonitor) throws IOException {
        this.pooledProcess = pooledProcess;
        this.service = service;
        this.serviceParameters = serviceParameters;
        this.processMonitor = processMonitor;
    }

    /**
     * Before executing the service, serialize all parameters in a json file
     */
    @Override
    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
        Process process = pooledProcess.process();
        processMonitor.monitor(process);

        // Serialize arguments in json file
        ObjectMapper mapper = new ObjectMapper();
        processMonitor.getRsocketServers().getServiceServer()
                .sendToEmitterSocket(new GwsPayload(GwsPayload.RUN_SERVICE, mapper.writeValueAsString(serviceParameters)).makePayload())
                .doOnError(e -> log.error("Error when requesting to run service ", e))
                .onErrorComplete()
                .block();

        process.waitFor();

        return RepeatStatus.FINISHED;
    }

}
