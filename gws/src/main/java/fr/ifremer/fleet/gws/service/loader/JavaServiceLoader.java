package fr.ifremer.fleet.gws.service.loader;

import fr.ifremer.fleet.gws.service.domain.GwsService;
import fr.ifremer.fleet.gws.service.domain.GwsServiceGroup;
import fr.ifremer.fleet.gws.service.repository.ServiceGroupRepository;
import fr.ifremer.fleet.gws.service.repository.ServiceRepository;
import fr.ifremer.fleet.gws.services.AbstractBasicService;
import fr.ifremer.fleet.gws.services.AbstractJarService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;

import java.io.*;
import java.net.URL;
import java.util.List;

/**
 * Loader of embedded services.<br>
 * Browse folder embedded-services in resources for service configuration files gws_conf.json<br>
 * Parse each gws_conf.json and create instances of GwsService
 */
@Configuration
@ComponentScan(includeFilters = @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = {AbstractJarService.class, AbstractBasicService.class}))
@Slf4j
public class JavaServiceLoader {

    /**
     * Context where to find all embedded services declared with @Component and implementing GwsEmbeddedService
     */
    @Autowired
    private ApplicationContext applicationContext;

    /**
     * Entry point, invoke when server starts.<br>
     * Search and load embedded services
     */
    @Bean
    ApplicationRunner loadEmbeddedServices(ServiceGroupRepository groupRepo, ServiceRepository serviceRepo) {
        return args -> {
            long serviceCount = 0;
            for (var serviceEntry : applicationContext.getBeansOfType(AbstractJarService.class).entrySet()) {
                loadEmbeddedService(serviceEntry.getKey(), serviceEntry.getValue(), groupRepo, serviceRepo);
                serviceRepo.flush();
            }
            for (var serviceEntry : applicationContext.getBeansOfType(AbstractBasicService.class).entrySet()) {
                loadEmbeddedService(serviceEntry.getKey(), serviceEntry.getValue(), groupRepo, serviceRepo);
                serviceRepo.flush();
            }
            log.info("Nb of embedded services : {}", serviceRepo.count() - serviceCount);
        };
    }

    /**
     * Creates a service with the information provided by an AbstractJarService
     */
    private void loadEmbeddedService(String serviceKey, AbstractJarService jarService, ServiceGroupRepository groupRepo, ServiceRepository serviceRepo) {
        log.info("Creating service {}", serviceKey);
        try {
            String bootstrap = extractJar(jarService.getJarBootstrap());

            var service = new GwsService();
            service.setName(jarService.getName());
            GwsServiceGroup serviceGroup = groupRepo.findOrCreateWithPath(jarService.getGroupPath());
            service.setParentGroup(serviceGroup);
            service.setGroupPath(service.getGroupPath());

            String javaExe = ProcessHandle.current().info().command().orElse("java");
            service.setCommand(List.of(javaExe, "-jar", bootstrap));
            service.setDescriptionPath(jarService.getDescriptionPath().toString());
            service.setHelpPath(jarService.getHelpPath().toString());

            serviceRepo.save(service);
        } catch (IOException e) {
            log.error("Error while loading service " + jarService.getName(), e);
        }
    }

    /**
     * Creates a service with the information provided by an AbstractBasicService
     */
    private void loadEmbeddedService(String serviceKey, AbstractBasicService basicService, ServiceGroupRepository groupRepo, ServiceRepository serviceRepo) {
        log.info("Creating service {}", serviceKey);
        GwsServiceGroup serviceGroup = groupRepo.findOrCreateWithPath(basicService.getGroupPath());

        var service = new GwsService();
        service.setName(basicService.getName());
        service.setParentGroup(serviceGroup);
        service.setGroupPath(service.getGroupPath());
        service.setCommand(List.of(serviceKey));
        service.setDescriptionPath(basicService.getDescriptionPath().toString());
        service.setHelpPath(basicService.getHelpPath().toString());

        serviceRepo.save(service);
    }

    /**
     * Extracts the jar to allow execution
     */
    private String extractJar(URL bootstrap) throws IOException {
        String fileName = bootstrap.getFile();
        if (fileName.lastIndexOf('/') > 0)
            fileName = fileName.substring(fileName.lastIndexOf('/') + 1);
        else
            fileName = fileName.hashCode() + ".jar"; // just in case...

        File outTempJar = new File("libs", fileName);
        outTempJar.getParentFile().mkdirs();
        outTempJar.delete();
        try (InputStream inJar = bootstrap.openStream(); OutputStream outJar = new FileOutputStream(outTempJar)) {
            inJar.transferTo(outJar);
        }
        return outTempJar.getCanonicalPath();
    }


}
