package fr.ifremer.fleet.gws.service.domain;

import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Group of services
 */
@Entity // Make a service ready for storage in a JPA-based data store
@Data // Generates getters, setters, equals, hashcode...
@NoArgsConstructor // Generates default constructor
public class GwsServiceGroup {

    /**
     * Unique id of a service
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * Description
     */
    private String name;

    /**
     * Path to this group (names of the parent groups)
     */
    private String path;

    /**
     * Link to parent group
     */
    @ManyToOne(fetch = FetchType.LAZY)
    private GwsServiceGroup parentGroup;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "parentGroup")
    private List<GwsServiceGroup> subGroups;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "parentGroup")
    private List<GwsService> services;

    /**
     * Constructor
     */
    public GwsServiceGroup(GwsServiceGroup parentGroup, String name) {
        this.name = name;
        this.path = (parentGroup != null ? parentGroup.getPath() : "") + "/" + name;
        this.parentGroup = parentGroup;
    }
}
