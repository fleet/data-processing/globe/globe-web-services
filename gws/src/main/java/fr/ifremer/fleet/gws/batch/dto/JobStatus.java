/**
 *
 */
package fr.ifremer.fleet.gws.batch.dto;

import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobExecution;

/**
 * Status of a job
 */
public enum JobStatus {
    PENDING, RUNNING, SUCCESS, ERROR, CANCELLED;

    public static JobStatus compute(JobExecution jobExecution) {
        if (jobExecution == null)
            return ERROR;
        if (ExitStatus.FAILED.equals(jobExecution.getExitStatus()))
            return ERROR;
        if (ExitStatus.STOPPED.equals(jobExecution.getExitStatus()))
            return CANCELLED;
        if (jobExecution.getStatus() == BatchStatus.STARTING)
            return PENDING;
        if (jobExecution.getStatus() == BatchStatus.COMPLETED)
            return SUCCESS;
        return RUNNING;

    }
}
