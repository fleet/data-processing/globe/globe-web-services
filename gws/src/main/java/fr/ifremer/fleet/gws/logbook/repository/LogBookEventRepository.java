package fr.ifremer.fleet.gws.logbook.repository;

import fr.ifremer.fleet.gws.logbook.domain.GwsLogBookEvent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

/**
 * Repository of GwsLogBookEvent
 */
public interface LogBookEventRepository extends JpaRepository<GwsLogBookEvent, Long> {
    @Query(value = "SELECT max(e.id) FROM GwsLogBookEvent e")
    Long getMaxId();

    /**
     * Delete all the completed events (status != PENDING and RUNNING)
     */
    @Modifying
    @Transactional
    @Query("delete from GwsLogBookEvent e where e.status>=2")
    void deleteAllButRunning();

    @Modifying
    @Transactional
    @Query("update GwsLogBookEvent e set e.status = 4 where e.status<2")
    void cancelAllRunning();
}
