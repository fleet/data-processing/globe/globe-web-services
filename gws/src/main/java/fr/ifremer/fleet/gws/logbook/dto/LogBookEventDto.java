package fr.ifremer.fleet.gws.logbook.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.PropertyNamingStrategies.SnakeCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import fr.ifremer.fleet.gws.batch.dto.JobStatus;
import fr.ifremer.fleet.gws.service.dto.ServiceDto;
import lombok.Builder;
import lombok.With;
import lombok.extern.slf4j.Slf4j;

import java.time.LocalDateTime;

/**
 * Object to transfer log book event data
 */
@Builder
@Slf4j
@JsonNaming(value = SnakeCaseStrategy.class)
@JsonInclude(JsonInclude.Include.NON_NULL)
public record LogBookEventDto(

        long id, // Unique id of an event
        String description,
        LocalDateTime start,
        LocalDateTime end,
        JobStatus status, // Execution status
        String error, // error message
        JsonNode parameters,
        JsonNode environments,
        String logs,
        @With ServiceDto service
) {
}
