package fr.ifremer.fleet.gws.logbook.domain;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.ifremer.fleet.gws.batch.dto.JobStatus;
import fr.ifremer.fleet.gws.logbook.dto.LogBookEventDto;
import jakarta.persistence.Entity;
import jakarta.persistence.Lob;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@Slf4j
public class GwsJobEvent extends GwsLogBookEvent {
    /**
     * Path to the service (/root/groups/...)
     * The name of the service is set in GwsLogBookEvent.description
     */
    private String groupPath;

    /**
     * End date
     */
    private LocalDateTime endDate;

    /**
     * How finished the event
     */
    private JobStatus status;

    /**
     * First intercepted message error.
     */
    private String error;

    /**
     * JSon parameters
     */
    @Lob
    private String parameters;

    public File getLogFile() {
        return new File("logs", getId().toString() + ".log");
    }

    @Override
    public LogBookEventDto mapToDto() {
        return LogBookEventDto.builder()
                .id(getId())
                .description(getDescription())
                .start(getDate())
                .end(getEndDate())
                .status(getStatus())
                .error(error != null && !error.isEmpty() ? error : "")
                .build();
    }

    @Override
    public LogBookEventDto mapToDetailedDto() {
        return LogBookEventDto.builder()
                .id(getId())
                .description(getDescription())
                .start(getDate())
                .end(getEndDate())
                .status(getStatus())
                .error(error != null && !error.isEmpty() ? error : "")
                .parameters(parseString(parameters))
                .logs(readLogs())
                .build();
    }

    private String readLogs() {
        File logFile = getLogFile();
        try {
            return Files.readString(logFile.toPath());
        } catch (IOException e) {
            log.warn("Error when reading the log file ", e.getMessage());
        }
        return "No log";
    }

    private JsonNode parseString(String value) {
        if (value != null && !value.isEmpty()) {
            try {
                return new ObjectMapper().readTree(value);
            } catch (IOException e) {
                log.warn("Error when parsing LogBookEvent parameters : {} ", e.getMessage());
            }
        }
        return null;
    }
}
