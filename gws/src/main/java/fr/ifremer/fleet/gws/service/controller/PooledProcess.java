package fr.ifremer.fleet.gws.service.controller;

import fr.ifremer.fleet.gws.rsocket.TwinnedRSocketServers;

/**
 * A running process waiting for a service to be executed. This service and its arguments are sent via an RSocket connection described in TwinnedRSocketServers
 */
public record PooledProcess(TwinnedRSocketServers servers, Process process) {
}
