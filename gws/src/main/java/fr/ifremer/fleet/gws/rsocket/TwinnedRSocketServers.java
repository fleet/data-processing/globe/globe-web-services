package fr.ifremer.fleet.gws.rsocket;

import io.rsocket.Payload;
import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import reactor.core.publisher.Mono;

/**
 * Bridges two RSocket servers (@link TwinRSocket). <br>
 * First one is managing a websocket and is dedicated to the client of GWS
 * (Globe)<br>
 * The other one is managing a TCP socket and is dedicated to the service
 * (Pyat)<br>
 * Any payload received from one server is transmitted to the other
 *
 * <pre>
 *                                                      +-----------------------+              +-----------------------+
 *                                                      | TwinRSocket Websocket |              |     TwinRSocket TCP   |
 *                                                      | {@link #clientServer} |              |{@link #serviceServer} |
 * +---------+              +------------+              +-----------------------+              +-----------------------+              +------------+              +---------+
 * |   GWS   | <--Payload-- |            | <--Payload-- |    responderSocket    | <--Payload-- |     emitterSocket     | <--Payload-- |            | <--Payload-- | Service |
 * |  client |              |  Websocket |              |                       |              |                       |              | TCP socket |              |  (Pyat) |
 * | (Globe) | --Payload--> |            | --Payload--> |     emitterSocket     | --Payload--> |    responderSocket    | --Payload--> |            | --Payload--> |         |
 * +---------+              +------------+              +-----------------------+              +-----------------------+              +------------+              +---------+
 * </pre>
 */
@Slf4j
public class TwinnedRSocketServers implements java.io.Closeable {
    /**
     * Client RSocket server
     */
    private final TwinRSocket clientServer;

    /**
     * Service RSocket server
     */
    private final TwinRSocket serviceServer;

    /**
     * Constructor.<br>
     * Instantiate and configure the twins
     */
    public TwinnedRSocketServers() {
        this.clientServer = new TwinRSocket(TransportType.WEB_SOCKET);
        this.serviceServer = new TwinRSocket(TransportType.TCP);
        clientServer.configure(this::forwardToTcpTwin, this::requestResponseFromTcpTwin);
        serviceServer.configure(this::forwardToWebsocketTwin, this::requestResponseFromWebsocketTwin);

        log.info("Bridge created between websocket port {} and TCP port {}", clientServer.getPort(),
                serviceServer.getPort());
    }

    /**
     * Reacts to a payload reception from websocket server by forwarding it to the
     * tcp twin
     */
    private Mono<Void> forwardToTcpTwin(Payload payload) {
        return serviceServer.sendToEmitterSocket(payload);
    }

    /**
     * Reacts to a payload reception from websocket server by forwarding it to the
     * tcp twin
     */
    private Mono<Payload> requestResponseFromTcpTwin(Payload payload) {
        return serviceServer.requestResponseFromEmitterSocket(payload);
    }

    /**
     * Reacts to a payload reception from tcp server by forwarding it to the
     * websocket twin
     */
    public Mono<Void> forwardToWebsocketTwin(Payload payload) {
        return clientServer.sendToEmitterSocket(payload);
    }

    /**
     * Reacts to a payload reception from websocket server by forwarding it to the
     * tcp twin
     */
    private Mono<Payload> requestResponseFromWebsocketTwin(Payload payload) {
        return clientServer.requestResponseFromEmitterSocket(payload);
    }

    /**
     * Return the host of the Websocket
     */
    public String getWebsocketHost() {
        return clientServer.getHost();
    }

    /**
     * Return the port of the Websocket
     */
    public int getWebsocketPort() {
        return clientServer.getPort();
    }

    /**
     * Return the TCP port of the first twin
     */
    public int getTcpPort() {
        return serviceServer.getPort();
    }

    public TwinRSocket getClientServer() {
        return clientServer;
    }

    public TwinRSocket getServiceServer() {
        return serviceServer;
    }


    /**
     * Closing the both servers
     */
    @Override
    public void close() {
        IOUtils.closeQuietly(clientServer);
        IOUtils.closeQuietly(serviceServer);
    }


}