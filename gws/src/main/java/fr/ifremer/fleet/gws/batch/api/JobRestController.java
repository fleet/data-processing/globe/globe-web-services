/**
 *
 */
package fr.ifremer.fleet.gws.batch.api;

import fr.ifremer.fleet.gws.batch.dto.JobDto;
import fr.ifremer.fleet.gws.batch.dto.JobStatus;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Controller the Jobs Rest API.
 */
@RestController
@RequestMapping("/gws/jobs")
@Slf4j
public class JobRestController {

    @Autowired
    private JobRepository jobRepository;

    /**
     * Return the JSon of all jobs present in the repository
     */
    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    List<JobDto> listAllJobs(@RequestParam(name = "status", required = false) JobStatus status) {
        return jobRepository.getJobNames().stream() //
                .flatMap(jobName -> jobRepository.findJobInstancesByName(jobName, 0, Integer.MAX_VALUE).stream())//
                .flatMap(jobInstance -> jobRepository.findJobExecutions(jobInstance).stream())//
                .map(JobDto::map)//
                .filter(job -> status == null || job.status() == status).toList();
    }

    /**
     * Return the JSon of a detailed job (with all available informations)
     */
    @GetMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<JobDto> getJobDetails(@PathVariable("id") Long id) {
        return jobRepository.getJobNames().stream() //
                .flatMap(jobName -> jobRepository.findJobInstancesByName(jobName, 0, Integer.MAX_VALUE).stream())//
                .flatMap(jobInstance -> jobRepository.findJobExecutions(jobInstance).stream())//
                .filter(jobExecution -> jobExecution.getId().equals(id))//
                .findFirst() //
                .map(JobDto::mapDetailed)//
                .map(ResponseEntity::ok)//
                .orElse(ResponseEntity.notFound()//
                        .build());
    }
}
