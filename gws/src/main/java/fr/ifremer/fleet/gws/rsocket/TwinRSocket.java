package fr.ifremer.fleet.gws.rsocket;

import io.rsocket.Payload;
import io.rsocket.RSocket;
import io.rsocket.core.RSocketServer;
import io.rsocket.transport.ServerTransport;
import io.rsocket.transport.netty.server.CloseableChannel;
import io.rsocket.transport.netty.server.TcpServerTransport;
import io.rsocket.transport.netty.server.WebsocketServerTransport;
import lombok.Getter;
import reactor.core.publisher.Mono;
import reactor.core.publisher.Sinks;

import java.io.Closeable;
import java.util.function.Function;

/**
 * One RSocket server belonging to a pair of Twinned RSocket server
 *
 * <pre>
 * In RSocket architecture, there are 4 RSockets involved in socket communication.
 * Here is represented the both RSocket of the server-side (GWS).
 * The same RSocket exists on the client-side (Service like Pyat for a TCP twin, client of the service like Globe for the WebSocket twin).
 *
 *                                                         +------------+                        +------------+
 *        ({@link #responderSocket})  <------Payload------ |            |  <------Payload------  |            |
 *                                                         |  Websocket |                        |    GWS     |
 *         ({@link #emitterSocket})   ------Payload------> |            |  ------Payload------>  |            |
 *                                                         +------------+                        +------------+
 * </pre>
 */
public class TwinRSocket implements Closeable {

    /**
     * TCP or Websocket
     */
    @Getter
    private final TransportType transportType;


    /**
     * RSocket used to open the socket and obtain the {@link #socketConnection}
     */
    private final RSocketServer connectionSocket;

    /**
     * Emitter part of the RSocket connection. Used to emit a request to client
     */
    private final Sinks.One<RSocket> emitterSocket = Sinks.one();

    /**
     * Interceptor of payload. Notified when a payload is emitted
     */
    private PayloadInterceptor payloadInterceptor = PayloadInterceptor.identity();

    /**
     * Channel of the socket to dispose to free the TCP/Websocket port
     */
    private CloseableChannel socketConnection;

    /**
     * Responder part of the RSocket connection when a request is received
     */
    private RSocket responderSocket;

    /**
     * Used to signal the client is connected to this server
     */
    private final Sinks.Empty<Void> onClientConnected = Sinks.empty();

    public TwinRSocket(TransportType transportType) {
        this.transportType = transportType;
        connectionSocket = RSocketServer.create();
    }

    public void configure(Function<Payload, Mono<Void>> fireAndForgetHandler,
                          Function<Payload, Mono<Payload>> requestResponseHandler) {
        socketConnection = connectionSocket//
                .acceptor((setup, sendingSocket) -> {
                    // server handles the setup for a new connection and creates a responder RSocket for accepting requests from the remote peer
                    this.emitterSocket.tryEmitValue(sendingSocket);
                    sendingSocket.onClose().doFinally(s -> close()).subscribe();
                    // Manage only fireAndForget request for this client
                    responderSocket = new RSocket() {
                        @Override
                        public Mono<Void> fireAndForget(Payload payload) {
                            return fireAndForgetHandler.apply(payload);
                        }

                        @Override
                        public Mono<Payload> requestResponse(Payload payload) {
                            return requestResponseHandler.apply(payload);
                        }
                    };

                    // Signal client is connected
                    onClientConnected.tryEmitEmpty();

                    return Mono.just(responderSocket);
                })//
                .bind(getServerTransport())//
                .block();
    }

    /**
     * Forward the payload to the requester
     */
    public Mono<Void> sendToEmitterSocket(Payload payload) {
        return emitterSocket.asMono().flatMap(socket ->
                socket.fireAndForget(payloadInterceptor.apply(payload)));
    }

    /**
     * Forward the payload to the emitter socket and wait for the response
     */
    public Mono<Payload> requestResponseFromEmitterSocket(Payload payload) {
        return emitterSocket.asMono().flatMap(socket -> socket.requestResponse(payload));
    }

    @Override
    public void close() {
        // Dispose all RSockets to allow Websocket closure
        if (responderSocket != null && !responderSocket.isDisposed())
            responderSocket.dispose();
        emitterSocket.asMono().subscribe(socket -> {
            if (!socketConnection.isDisposed()) socket.dispose();
        });
        // Close the socket
        if (socketConnection != null)
            socketConnection.dispose();
    }

    /**
     * Return the ServerTransport depending on the TransportType
     */
    private ServerTransport<CloseableChannel> getServerTransport() {
        return transportType == TransportType.TCP//
                ? TcpServerTransport.create("0.0.0.0", 0 /* Any free port */)//
                : WebsocketServerTransport.create("0.0.0.0", 0 /* Any free port */);
    }

    /**
     * Return the bound host
     */
    public String getHost() {
        return socketConnection != null ? socketConnection.address().getHostString() : "localhost";
    }

    /**
     * Return the bound port
     */
    public int getPort() {
        return socketConnection != null ? socketConnection.address().getPort() : 0;
    }

    public void setPayloadInterceptor(PayloadInterceptor payloadInterceptor) {
        this.payloadInterceptor = payloadInterceptor;
    }

    public RSocketServer getConnectionSocket() {
        return connectionSocket;
    }

    /**
     * Signal when the client is connected
     */
    public Mono<Void> onClientConnected() {
        return onClientConnected.asMono();
    }

    /**
     * Signal when the client disconnects
     */
    public Mono<Void> onClose() {
        return socketConnection != null ? socketConnection.onClose() : Mono.empty();
    }

}
