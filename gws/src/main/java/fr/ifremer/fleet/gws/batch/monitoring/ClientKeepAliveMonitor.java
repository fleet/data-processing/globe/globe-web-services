package fr.ifremer.fleet.gws.batch.monitoring;

import fr.ifremer.fleet.gws.service.repository.ServiceGroupRepository;
import fr.ifremer.fleet.gws.service.repository.ServiceRepository;
import io.rsocket.RSocket;
import io.rsocket.core.RSocketClient;
import io.rsocket.core.RSocketConnector;
import io.rsocket.transport.netty.client.TcpClientTransport;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;
import reactor.util.retry.Retry;

import java.time.Duration;
import java.util.List;

/**
 * Controller maintaining a lifeline (Rsocket) with the client (Globe).
 * As soon as the connection is lost, GWS is shutdown
 */
@Slf4j
@Component
public class ClientKeepAliveMonitor {

    @Bean
    private ApplicationRunner openLifeLine(ServiceGroupRepository groupRepo, ServiceRepository serviceRepo) {
        return args -> {
            List<String> ports = args.getOptionValues("keep_alive_port");
            if (ports != null && !ports.isEmpty()) {
                int port = NumberUtils.toInt(ports.get(0), -1);
                if (port > 0) {
                    // Instance receiving payloads. Responding by a UnsupportedInteractionException (no payload expected)
                    RSocket responder = new RSocket() {
                    };

                    // Defines the TCP connection
                    Mono<RSocket> source =
                            RSocketConnector.create()
                                    .acceptor((setup, sendingSocket) -> {
                                        log.info("Keep alive port '{}' opened", ports.get(0));
                                        sendingSocket.onClose().doFinally(s -> shutdownGws()).subscribe();
                                        return Mono.just(responder);
                                    })
                                    .keepAlive(Duration.ofSeconds(5L), Duration.ofSeconds(90L))
                                    .reconnect(Retry.backoff(5, Duration.ofMillis(500)))
                                    .connect(TcpClientTransport.create("localhost", port))
                                    .doOnError(exc -> {
                                        // Connection error (Connection retries exhausted)
                                        log.error("Keep alive error : {}.", exc.getMessage());
                                        shutdownGws();
                                    });

                    // Connects to client
                    RSocketClient client = RSocketClient.from(source);
                    if (!client.connect())
                        log.warn("Connection failed on keep alive port '{}'", ports.get(0));
                }
            } else log.info("No keep alive configured");
        };
    }

    private void shutdownGws() {
        log.warn("Server shutdown initiated.");
        System.exit(1);
    }
}
