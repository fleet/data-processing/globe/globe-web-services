package fr.ifremer.fleet.gws.service.repository;

import fr.ifremer.fleet.gws.service.domain.GwsServiceGroup;
import org.springframework.data.jpa.repository.JpaRepository;

import java.io.File;
import java.util.List;
import java.util.Optional;

/**
 * Repository of GwsServiceGroup, automatically generates a CRUD for groups
 */


public interface ServiceGroupRepository extends JpaRepository<GwsServiceGroup, Long> {

    List<GwsServiceGroup> findByName(String lastName);

    List<GwsServiceGroup> findByPath(String path);

    List<GwsServiceGroup> findByParentGroupAndName(GwsServiceGroup parentGroup, String name);

    default GwsServiceGroup getRootGroup() {
        List<GwsServiceGroup> roots = findByName("root");
        Optional<GwsServiceGroup> rootOptional = roots.stream().filter(r -> r.getParentGroup() == null).findFirst();
        if (rootOptional.isPresent()) {
            return rootOptional.get();
        }

        // Root not found
        var rootGroup = new GwsServiceGroup(null, "root");
        return save(rootGroup);
    }

    default GwsServiceGroup findOrCreateSubGroupByName(GwsServiceGroup parentGroup, String subGroupName) {
        List<GwsServiceGroup> subGroups = findByParentGroupAndName(parentGroup, subGroupName);
        if (subGroups.isEmpty()) {
            // Unknown group
            var result = new GwsServiceGroup(parentGroup, subGroupName);
            return save(result);
        }
        return subGroups.get(0);
    }

    default GwsServiceGroup findOrCreateWithPath(String path) {
        if (path == null || path.isEmpty() || path.equals("root") || path.equals("/root"))
            return getRootGroup();

        List<GwsServiceGroup> subGroups = findByPath(path);
        if (subGroups.isEmpty()) {
            // Unknown group
            String groupName = new File(path).getName();
            String parentGroupPath = path.substring(0, path.length() - groupName.length() - 1);
            GwsServiceGroup parentGroup = findOrCreateWithPath(parentGroupPath);
            var result = new GwsServiceGroup(parentGroup, groupName);
            return save(result);
        }
        return subGroups.get(0);
    }

}
