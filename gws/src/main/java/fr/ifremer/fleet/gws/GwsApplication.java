package fr.ifremer.fleet.gws;

import com.zaxxer.hikari.HikariDataSource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.sql.DataSource;

/**
 * Bootstrap and launch the Spring application. <br>
 * Fire up a servlet container and serve up services
 */
@SpringBootApplication
@RestController
@RequestMapping("/gws")
@Slf4j
public class GwsApplication {
    public static void main(String[] args) {
        try {
            SpringApplication.run(GwsApplication.class, args);
        } catch (Throwable t) {
            log.error("GWS not started : '{}'", t.getMessage());
        }
    }

    @GetMapping("/version")
    public String version() {
        return "0.1.0";
    }

    @Bean
    @Primary
    @ConfigurationProperties(prefix = "gws.datasource")
    /** Configure an H2 database to store the business objects */
    public DataSource dataSource() {
        HikariDataSource result = DataSourceBuilder.create().type(HikariDataSource.class).build();
        result.setMaximumPoolSize(150);
        return result;
    }

}
