/**
 *
 */
package fr.ifremer.fleet.gws.logbook.api;

import fr.ifremer.fleet.gws.logbook.domain.GwsJobEvent;
import fr.ifremer.fleet.gws.logbook.domain.GwsLogBookEvent;
import fr.ifremer.fleet.gws.logbook.dto.LogBookEventDto;
import fr.ifremer.fleet.gws.logbook.repository.LogBookEventRepository;
import fr.ifremer.fleet.gws.service.dto.ServiceDto;
import fr.ifremer.fleet.gws.service.repository.ServiceRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Controller the Rest API for services.
 */
@RestController
@RequestMapping("/gws/logbook")
@Slf4j
public class LogBookRestController {

    /**
     * Repository of log book events
     */
    @Autowired
    private LogBookEventRepository logBookRepo;

    /**
     * Repository of log book events
     */
    @Autowired
    private ServiceRepository serviceRepo;

    /**
     * Return the JSon of the Log book
     */
    @GetMapping("")
    List<LogBookEventDto> listAllLogBookEvent() {
        return logBookRepo.findAll().stream()
                .map(GwsLogBookEvent::mapToDto)
                .toList();
    }

    /**
     * Delete all the completed events
     */
    @DeleteMapping("")
    void deleteAllLogBookEvent() {
        logBookRepo.deleteAllButRunning();
        List<GwsLogBookEvent> allEvents = logBookRepo.findAll();

        // Clean the logs folder
        Set<File> filesToKeep = allEvents.stream().filter(GwsJobEvent.class::isInstance).map(GwsJobEvent.class::cast).map(GwsJobEvent::getLogFile).collect(Collectors.toSet());
        File[] filesToDelete = new File("logs").listFiles(f -> !filesToKeep.contains(f));
        Arrays.stream(filesToDelete).forEach(File::delete);
    }

    /**
     * Return the JSon of a detailed log book event
     */
    @GetMapping("/{id}")
    public ResponseEntity<LogBookEventDto> getLogBookEventDetails(@PathVariable("id") Long id) {
        GwsLogBookEvent logBookEvent = logBookRepo.findById(id).orElse(null);
        if (logBookEvent instanceof GwsJobEvent jobEvent) {

            ServiceDto service = serviceRepo.findByGroupPathAndName(jobEvent.getGroupPath(), jobEvent.getDescription())
                    .map(ServiceDto::mapWithDescription)
                    .orElse(null);
            if (service != null) {
                return logBookRepo.findById(id)
                        .map(GwsLogBookEvent::mapToDetailedDto)
                        .filter(Objects::nonNull)
                        .map(event -> event.withService(service))
                        .map(ResponseEntity::ok)
                        .orElse(ResponseEntity.notFound()
                                .build());
            }
        }
        return ResponseEntity.unprocessableEntity().build();
    }

    /**
     * Delete an event
     */
    @DeleteMapping("/{id}")
    void deleteOneLogBookEvent(@PathVariable("id") Long id) {
        logBookRepo.deleteById(id);
        new File("logs", id + ".log").delete();
    }
}
