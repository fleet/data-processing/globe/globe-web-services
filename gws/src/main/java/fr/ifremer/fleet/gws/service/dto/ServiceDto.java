package fr.ifremer.fleet.gws.service.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategies.SnakeCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import fr.ifremer.fleet.gws.service.domain.GwsService;
import lombok.Builder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.UrlResource;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Object to transfer Service data
 */
@Builder
@Slf4j
@JsonNaming(value = SnakeCaseStrategy.class)
@JsonInclude(JsonInclude.Include.NON_NULL)
public record ServiceDto(

        long id, // Unique id of a service
        String name,
        String groupPath,
        String configurationPath, // Description file path
        JsonNode description, // /Content of the description file
        String help, // Help text in HTML format
        String helpPath // Help file path
) {

    /**
     * Map a GwsService to its DTO
     */
    public static ServiceDto map(GwsService service) {
        return ServiceDto.builder()//
                .id(service.getId())//
                .groupPath(service.getGroupPath()) //
                .name(service.getName()) //
                .configurationPath(service.getDescriptionPath()) //
                .build();
    }

    /**
     * Map a GwsService to its DTO
     */
    public static ServiceDto mapWithDescription(GwsService service) {
        return ServiceDto.builder()//
                .id(service.getId())//
                .name(service.getName()) //
                .description(parseJsonFile(service.getDescriptionPath()))//
                .build();
    }

    /**
     * Map a GwsService to its DTO
     */
    public static ServiceDto mapDetailed(GwsService service) {
        return ServiceDto.builder()//
                .id(service.getId())//
                .name(service.getName()) //
                .description(parseJsonFile(service.getDescriptionPath()))//
                .help(readFile(service.getHelpPath()))//
                .helpPath(readPath(service.getHelpPath()))//
                .build();
    }

    /**
     * Read a file and return the content
     */
    private static String readFile(String path) {
        if (path != null && !path.isEmpty()) {
            try {
                if (path.startsWith("jar:") || path.startsWith("file:"))
                    return UrlResource.from(path).getContentAsString(StandardCharsets.UTF_8);
                return Files.readString(Path.of(path));
            } catch (IOException e) {
                log.warn("Error when reading the file {} : {} ", path, e.getMessage());
            }
        }
        return "";
    }

    /**
     * Parse a filepath and return it as a valid URL
     */
    private static String readPath(String path) {
        if (path != null && !path.isEmpty()  && !path.startsWith("jar:") && !path.startsWith("file:"))
            return path;
        return "";
    }

    /**
     * Read a JSon file and return the JsonNode
     */
    private static JsonNode parseJsonFile(String path) {
        if (path != null && !path.isEmpty()) {
            try {
                if (path.startsWith("jar:") || path.startsWith("file:"))
                    return new ObjectMapper().readTree(new URL(path));
                return new ObjectMapper().readTree(new File(path));
            } catch (IOException e) {
                log.warn("Error when reading the file {} : {} ", path, e.getMessage());
            }
        }
        return null;
    }

}
