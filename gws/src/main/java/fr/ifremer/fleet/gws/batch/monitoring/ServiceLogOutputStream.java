package fr.ifremer.fleet.gws.batch.monitoring;

import fr.ifremer.fleet.gws.rsocket.GwsPayload;
import fr.ifremer.fleet.gws.rsocket.TwinnedRSocketServers;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.exec.LogOutputStream;

import java.time.Instant;

/**
 * This class aim at redirecting output streams of a process to WebSocket and log file
 */
@Slf4j
public class ServiceLogOutputStream extends LogOutputStream {

    private final TwinnedRSocketServers servers;

    /**
     * Constructor
     */
    public ServiceLogOutputStream(int logLevel, TwinnedRSocketServers servers) {
        super(logLevel);
        this.servers = servers;
    }


    /**
     * One line has been received from the one process stream. <br>
     * Forwards it to WebSocket
     */
    @Override
    protected void processLine(String line, int logLevel) {
        var logPayload = GwsPayload.makeLogPayload(logLevel, line, Instant.now().toString(), "");
        servers.forwardToWebsocketTwin(logPayload.makePayload()).onErrorComplete().block();
    }

}
