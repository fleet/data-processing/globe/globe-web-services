/**
 *
 */
package fr.ifremer.fleet.gws.service.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.IntNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.TextNode;
import fr.ifremer.fleet.gws.batch.PooledProcessTasklet;
import fr.ifremer.fleet.gws.batch.ProcessTasklet;
import fr.ifremer.fleet.gws.batch.monitoring.ServiceProcessMonitor;
import fr.ifremer.fleet.gws.rsocket.GwsPayload;
import fr.ifremer.fleet.gws.rsocket.TwinnedRSocketServers;
import fr.ifremer.fleet.gws.rsocket.flatbuffers.LogLevel;
import fr.ifremer.fleet.gws.service.domain.GwsService;
import fr.ifremer.fleet.gws.services.AbstractBasicService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.*;
import org.springframework.batch.core.job.builder.JobBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.step.builder.StepBuilder;
import org.springframework.batch.core.step.tasklet.MethodInvokingTaskletAdapter;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.PlatformTransactionManager;

import java.io.IOException;
import java.time.Instant;
import java.util.*;

/**
 * Controller of service. <br>
 * Creates a job when a launch of service is requested
 */
@Slf4j
@Controller
public class ServiceManager {

    /**
     * Injection of the server HTTP port
     */
    @Value("${server.port}")
    private int serverPort;

    /**
     * Job parameter name associated to the id of the service
     */
    public static final String JOB_PARAM_SERVICE_ID = "service_id";
    /**
     * Job parameter name associated to the name of the service
     */
    public static final String JOB_PARAM_SERVICE_NAME = "service_name";
    /**
     * Job parameter name associated to the RSocket port opened for service (Pyat)
     */
    public static final String JOB_PARAM_TCP_PORT = "tcp_port";
    /**
     * Job parameter name associated to the RSocket websocket host name opened for
     * service client (Globe)
     */
    public static final String JOB_PARAM_WEBSOCKET_HOST = "websocket_address";
    /**
     * Job parameter name associated to the RSocket websocket port opened for
     * service client (Globe)
     */
    public static final String JOB_PARAM_WEBSOCKET_PORT = "websocket_port";

    private final JobRepository jobRepository;
    private final JobLauncher jobLauncher;
    private final PlatformTransactionManager transactionManager;

    /**
     * Context where to find all embedded services declared with @Component and implementing GwsEmbeddedService
     */
    private final ApplicationContext applicationContext;

    /**
     * All process pools. Index is the the hash code of the command line used to run all processes of the pool
     */
    private final Map<Integer, ProcessPool> processPools = new HashMap<>();

    @Autowired
    public ServiceManager(JobRepository jobRepository, JobLauncher jobLauncher, PlatformTransactionManager transactionManager, ApplicationContext applicationContext) {
        this.jobRepository = jobRepository;
        this.jobLauncher = jobLauncher;
        this.transactionManager = transactionManager;
        this.applicationContext = applicationContext;
    }


    public void initializeServicePool(String[] command, int initialSize) {
        ProcessPool processPool = new ProcessPool(command, initialSize);
        processPools.put(processPool.hashCode(), processPool);
    }

    /**
     * Launch the execution of the service in a job
     */
    public JobParameters launch(GwsService service, JsonNode serviceParameters, boolean recordToLogBook) throws JobExecutionException {
        // Check if it's possible to use one of the PooledProcess
        List<String> processCommandLine = service.getCommand();
        int commandKey = processCommandLine.hashCode();
        // If not null, a ProcessPool is suitable for running the service
        ProcessPool processPool = processPools.get(commandKey);

        return launchService(Optional.ofNullable(processPool), service, serviceParameters, recordToLogBook);
    }

    /**
     * Request the execution of the service in a job.
     * If processPool is present, the job will request execution of the service in a PooledProcess.
     * Otherwise, a new process will be created
     */
    private JobParameters launchService(Optional<ProcessPool> processPool, GwsService service, JsonNode serviceParameters, boolean recordToLogBook) throws JobExecutionException {

        try {
            // Get one PooledProcess from the pool if present
            Optional<PooledProcess> pooledProcess = processPool.flatMap(ProcessPool::getPooledService);

            // Get the RSocket servers from the pooled process if present. Otherwise, creates new
            TwinnedRSocketServers servers = pooledProcess.map(PooledProcess::servers).orElseGet(TwinnedRSocketServers::new);

            // Process requires the http port
            if (serviceParameters.get("gws_http_port") instanceof IntNode)
                ((ObjectNode) serviceParameters).replace("gws_http_port", new IntNode(serverPort));

            // Add a JSon node with the socket port to the set of parameters
            if (pooledProcess.isEmpty())
                ((ObjectNode) serviceParameters).set("rsocket_port", new IntNode(servers.getTcpPort()));
            ((ObjectNode) serviceParameters).set("configuration_file", new TextNode(service.getDescriptionPath()));

            JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
            // Use a UUID to identify a JobExecution. (Batch will refuse to run two jobs with the same parameters)
            jobParametersBuilder.addString("uuid", UUID.randomUUID().toString(), true);
            jobParametersBuilder.addLong(JOB_PARAM_SERVICE_ID, service.getId(), false);
            jobParametersBuilder.addString(JOB_PARAM_SERVICE_NAME, service.getName(), false);
            jobParametersBuilder.addString(JOB_PARAM_SERVICE_NAME, service.getName(), false);
            jobParametersBuilder.addString(JOB_PARAM_WEBSOCKET_HOST, servers.getWebsocketHost(), false);
            jobParametersBuilder.addLong(JOB_PARAM_WEBSOCKET_PORT, (long) servers.getWebsocketPort(), false);
            jobParametersBuilder.addLong(JOB_PARAM_TCP_PORT, (long) servers.getTcpPort(), false);
            JobParameters jobParameters = jobParametersBuilder.toJobParameters();

            // Job will be created and run as soon as the client is connected to the socket
            servers.getClientServer().onClientConnected().doFinally(v -> runJob(pooledProcess, service, serviceParameters, recordToLogBook, jobParameters, servers)).subscribe();
            return jobParameters;

        } catch (Exception e) {
            throw new JobExecutionException("Unable to launch the service " + service.getName(), e);
        }
    }

    /**
     * The client has just connect to the socket.
     * A Spring Batch job can now be created and run.
     * One of the job steps will be responsible for managing the Process (attached to the PooledProcess if present. Otherwise, creates a new one).
     */
    private void runJob(Optional<PooledProcess> pooledProcess, GwsService service, JsonNode serviceParameters, boolean recordToLogBook, JobParameters jobParameters, TwinnedRSocketServers servers) {
        log.info("Client is connected. Run the job '{}'", service.getName());

        try {
            ServiceProcessMonitor processMonitor = new ServiceProcessMonitor(service, serviceParameters, servers, recordToLogBook);
            Job job = new JobBuilder(service.getName() + "_job", jobRepository)//
                    .start(execServiceStep(pooledProcess, service, serviceParameters, processMonitor))//
                    .listener(processMonitor)
                    .build();
            jobLauncher.run(job, jobParameters);
        } catch (Exception e) {
            String message = "Unable to run the job " + service.getName() + "(" + e.getMessage() + ")";
            log.error(message, e);
            servers.getClientServer()
                    .sendToEmitterSocket(GwsPayload.makeLogPayload(LogLevel.ERROR, message, Instant.now().toString(), "").makePayload())
                    .and(v -> servers.close())
                    .subscribe();
        }
    }


    /**
     * Return the step of the job for running the service
     * If pooled process is present, the job will request execution of the service with it.
     * Otherwise, a new process will be created
     */
    private Step execServiceStep(Optional<PooledProcess> pooledProcess, GwsService service, JsonNode serviceParameters, ServiceProcessMonitor processMonitor)
            throws IOException {
        //
        List<String> command = service.getCommand();

        try {
            // Check if Embedded service.
            if (pooledProcess.isEmpty() && command.size() == 1) {
                // Embedded service : call AbstractBasicService.run method directly
                AbstractBasicService embeddedService = applicationContext.getBean(command.get(0), AbstractBasicService.class);
                MethodInvokingTaskletAdapter methodInvokingTaskletAdapter = new MethodInvokingTaskletAdapter();
                methodInvokingTaskletAdapter.setTargetObject(embeddedService);
                methodInvokingTaskletAdapter.setTargetMethod("run");
                methodInvokingTaskletAdapter.setArguments(new Object[]{serviceParameters});
                return new StepBuilder(service.getName() + "_Step", jobRepository)//
                        .tasklet(methodInvokingTaskletAdapter, transactionManager)//
                        .allowStartIfComplete(true) //
                        .build();
            }
        } catch (BeansException e) {
            // getBean() failed. Not an embedded service...
        }

        // The job step delegates the processing of the service to a Tasklet
        return new StepBuilder(service.getName() + "_Step", jobRepository)//
                .tasklet(newServiceTasklet(pooledProcess, service, serviceParameters, processMonitor), transactionManager)//
                .allowStartIfComplete(true) //
                .build();


    }

    /**
     * Return the Tasklet of the step for running the service
     * If pooled process is present, the type of returned tasklet is PooledProcessTasklet
     * Otherwise, the type of returned tasklet is ProcessTasklet
     */
    private Tasklet newServiceTasklet(Optional<PooledProcess> pooledProcess, GwsService service, JsonNode serviceParameters, ServiceProcessMonitor processMonitor) throws IOException {
        if (pooledProcess.isPresent()) {
            log.info("Preparing a Tasklet with port {} and tcp port {}", processMonitor.getRsocketServers().getWebsocketPort(), processMonitor.getRsocketServers().getTcpPort());
            return new PooledProcessTasklet(pooledProcess.get(), service, serviceParameters, processMonitor);
        }

        var result = new ProcessTasklet(service, serviceParameters, processMonitor);
        // No timeout
        result.setTimeout(Long.MAX_VALUE);
        return result;
    }
}
