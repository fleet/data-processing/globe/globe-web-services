package fr.ifremer.fleet.gws.services;

import com.fasterxml.jackson.databind.JsonNode;
import fr.ifremer.fleet.gws.rsocket.GwsPayload;
import fr.ifremer.fleet.gws.service.GwsSocketForService;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Marker;

import java.net.URL;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Plain service executed directly by GWS
 */
@Data
@Slf4j
@RequiredArgsConstructor
public abstract class AbstractBasicService {

    /**
     * Name of the service. Displayed in th Service tool explorer of Globe
     */
    private final String name;
    /**
     * Service group path
     */
    private final String groupPath;
    /**
     * URL in resource of the json file describing all arguments expected by the service.
     */
    private final URL descriptionPath;
    /**
     * URL in resource of the html help file
     */
    private final URL helpPath;

    /**
     * Invoked by GWS to run the service
     */
    public void run(JsonNode parameters) {
        AtomicBoolean cancelFlag = new AtomicBoolean(false);
        Optional<GwsSocketForService> socket = Optional.empty();
        Marker logMarker = null;
        try {
            JsonNode portNode = parameters.findValue("rsocket_port");
            if (portNode != null) {
                int port = parameters.findValue("rsocket_port").asInt();
                JsonNode hostNode = parameters.findValue("rsocket_host");
                String host = hostNode != null ? hostNode.asText() : "localhost";
                GwsSocketForService gwsSocket = new GwsSocketForService(host, port, gwsPayload -> processPayload(gwsPayload, cancelFlag));
                logMarker = gwsSocket.getLogMarker();
                socket = Optional.of(gwsSocket);
            } else {
                log.warn("No rsocket_port in parameters. RSocket feature discarded");
            }
            doRun(new RunningContext(parameters, socket, logMarker, cancelFlag));
        } catch (Throwable t) {
            log.error(logMarker, "Service failed with exception", t);
        } finally {
            socket.ifPresent(GwsSocketForService::dispose);
        }
    }

    /**
     * React to the reception of a payload
     */
    private void processPayload(GwsPayload gwsPayload, AtomicBoolean cancelFlag) {
        if ("cancel".equals(gwsPayload.route()))
            cancelFlag.set(true);
        else
            log.info("Payload {} received but ignored", gwsPayload.route());
    }

    /**
     * Internal run.
     */
    protected abstract void doRun(RunningContext context);

    public record RunningContext(
            // Json parameters from the client
            JsonNode parameters,
            // Opened RSocket to communicate with the client
            Optional<GwsSocketForService> socket,
            // Marker used to mark log for the client
            Marker logMarker,
            // True when cancel is required
            AtomicBoolean cancelRequired) {
    }
}
