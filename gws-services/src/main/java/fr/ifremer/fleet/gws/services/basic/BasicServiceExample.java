package fr.ifremer.fleet.gws.services.basic;

import com.fasterxml.jackson.databind.JsonNode;
import fr.ifremer.fleet.gws.service.GwsSocketForService;
import fr.ifremer.fleet.gws.services.AbstractBasicService;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Marker;
import org.springframework.stereotype.Component;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Example of service executed directly by GWS
 */
@Slf4j
@Component("one name to identify this service")
public class BasicServiceExample extends AbstractBasicService {
    public BasicServiceExample() {
        super("Basic cancellable Java service" // name
                , "/root/toolbox_services/Experimental for GWS integration" // Group
                , BasicServiceExample.class.getResource("/services/basic_service_example.json") // Description
                , BasicServiceExample.class.getResource("/services/help/basic_service_example.html") // Help
        );
    }

    @Override
    public void doRun(RunningContext context) {
        JsonNode parameters = context.parameters();
        GwsSocketForService socket = context.socket().orElse(null);
        Marker logMarker = context.logMarker();
        AtomicBoolean cancelRequired = context.cancelRequired();

        int delay = parameters.findValue("delay").asInt();
        log.info(logMarker, "Waiting for cancel during {}s !", delay);

        int total = delay * 2;
        int countDown = total;
        while (!cancelRequired.get() && countDown > 0) {
            try {
                Thread.sleep(500L);
                countDown--;
                if (socket != null) {
                    float progression = 1f - (float) countDown / total;
                    socket.sendProgression(progression, "Waiting progression " + (int) (100f * progression) + "%");
                } else
                    log.info(logMarker, "Wait..");
            } catch (InterruptedException e) {
                // Never mind
            }
        }
        if (cancelRequired.get())
            log.info(logMarker, "Cancel successfully received.");
        else
            log.info(logMarker, "Cancel not received.");
    }
}
