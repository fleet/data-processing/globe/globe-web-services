package fr.ifremer.fleet.gws.services.sonarnetcdf;

import fr.ifremer.fleet.gws.services.AbstractJarService;
import fr.ifremer.fleet.gws.services.basic.BasicServiceExample;
import org.springframework.stereotype.Component;

/**
 * Service declaring the sonar-netcdf-converter*.jar to GWS allowing the conversion to MBG
 */
@Component
public class MbgConverterService extends AbstractJarService {
    public MbgConverterService() {
        super("Convert sounding file to MBG" // name
                , "/root/toolbox_services/Convert" // Group
                , BasicServiceExample.class.getResource("/services/mbg_converter.json") // Description
                , BasicServiceExample.class.getResource("/services/help/convert_to_mbg.html") // Help
                , "sonar-netcdf-converter*.jar" // Jar file name
                , "fr.ifremer.globe.soundingsfileconverter.SonarNetcdfConverter" // Class name in sonar-netcdf-converter*.jar
        );
    }

}
