package fr.ifremer.fleet.gws.services;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import java.net.URL;

/**
 * Service provided through jar execution
 */
@Data
@Slf4j
@RequiredArgsConstructor
public abstract class AbstractJarService {
    /**
     * Name of the service. Displayed in th Service tool explorer of Globe
     */
    private final String name;
    /**
     * Service group path
     */
    private final String groupPath;
    /**
     * URL in resource of the json file describing all arguments expected by the service.
     */
    private final URL descriptionPath;
    /**
     * URL in resource of the html help file
     */
    private final URL helpPath;

    /**
     * Jar file name used for bootstrapping the services. Can contain wildcard character
     */
    private final String jarFileRegex;

    /**
     * Name of a class in the jar file. Useful when GWS is launched from IntelliJ and the jar file is not in BOOT-INF/lib/ resources
     */
    private final String classNameInJar;

    public URL getJarBootstrap() {
        return locateJar(jarFileRegex, classNameInJar);
    }

    /**
     * Browse BOOT-INF/lib/ resources to find the bootstrap jar file
     *
     * @param jarFileName Jar file name used for bootstrapping the services. Can contain wildcard character
     */
    private URL locateJar(String jarFileName, String orClassInJar) {
        PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
        try {
            // Search the corresponding bootstrap jar in BOOT-INF/lib/ resources
            Resource[] jarResources = resolver.getResources("BOOT-INF/lib/" + jarFileName);
            if (jarResources != null && jarResources.length > 0)
                return jarResources[0].getURL();
        } catch (Exception e) {
            log.warn(e.getMessage());
        }

        //
        try {
            return getClass().getClassLoader().loadClass(orClassInJar).getProtectionDomain().getCodeSource().getLocation();
        } catch (ClassNotFoundException e) {
            // We are in debug mode. Throwing Exception is acceptable
            throw new RuntimeException(e);
        }
    }
}
