// automatically generated by the FlatBuffers compiler, do not modify

import * as flatbuffers from 'flatbuffers';

export class XsfAcquisitionMode {
  bb: flatbuffers.ByteBuffer|null = null;
  bb_pos = 0;
  __init(i:number, bb:flatbuffers.ByteBuffer):XsfAcquisitionMode {
  this.bb_pos = i;
  this.bb = bb;
  return this;
}

static getRootAsXsfAcquisitionMode(bb:flatbuffers.ByteBuffer, obj?:XsfAcquisitionMode):XsfAcquisitionMode {
  return (obj || new XsfAcquisitionMode()).__init(bb.readInt32(bb.position()) + bb.position(), bb);
}

static getSizePrefixedRootAsXsfAcquisitionMode(bb:flatbuffers.ByteBuffer, obj?:XsfAcquisitionMode):XsfAcquisitionMode {
  bb.setPosition(bb.position() + flatbuffers.SIZE_PREFIX_LENGTH);
  return (obj || new XsfAcquisitionMode()).__init(bb.readInt32(bb.position()) + bb.position(), bb);
}

modeCount():number {
  const offset = this.bb!.__offset(this.bb_pos, 4);
  return offset ? this.bb!.readInt32(this.bb_pos + offset) : 0;
}

modeName(index: number):string
modeName(index: number,optionalEncoding:flatbuffers.Encoding):string|Uint8Array
modeName(index: number,optionalEncoding?:any):string|Uint8Array|null {
  const offset = this.bb!.__offset(this.bb_pos, 6);
  return offset ? this.bb!.__string(this.bb!.__vector(this.bb_pos + offset) + index * 4, optionalEncoding) : null;
}

modeNameLength():number {
  const offset = this.bb!.__offset(this.bb_pos, 6);
  return offset ? this.bb!.__vector_len(this.bb_pos + offset) : 0;
}

pingCount():number {
  const offset = this.bb!.__offset(this.bb_pos, 8);
  return offset ? this.bb!.readInt32(this.bb_pos + offset) : 0;
}

modeIndex(index: number):number|null {
  const offset = this.bb!.__offset(this.bb_pos, 10);
  return offset ? this.bb!.readInt32(this.bb!.__vector(this.bb_pos + offset) + index * 4) : 0;
}

modeIndexLength():number {
  const offset = this.bb!.__offset(this.bb_pos, 10);
  return offset ? this.bb!.__vector_len(this.bb_pos + offset) : 0;
}

modeIndexArray():Int32Array|null {
  const offset = this.bb!.__offset(this.bb_pos, 10);
  return offset ? new Int32Array(this.bb!.bytes().buffer, this.bb!.bytes().byteOffset + this.bb!.__vector(this.bb_pos + offset), this.bb!.__vector_len(this.bb_pos + offset)) : null;
}

static startXsfAcquisitionMode(builder:flatbuffers.Builder) {
  builder.startObject(4);
}

static addModeCount(builder:flatbuffers.Builder, modeCount:number) {
  builder.addFieldInt32(0, modeCount, 0);
}

static addModeName(builder:flatbuffers.Builder, modeNameOffset:flatbuffers.Offset) {
  builder.addFieldOffset(1, modeNameOffset, 0);
}

static createModeNameVector(builder:flatbuffers.Builder, data:flatbuffers.Offset[]):flatbuffers.Offset {
  builder.startVector(4, data.length, 4);
  for (let i = data.length - 1; i >= 0; i--) {
    builder.addOffset(data[i]!);
  }
  return builder.endVector();
}

static startModeNameVector(builder:flatbuffers.Builder, numElems:number) {
  builder.startVector(4, numElems, 4);
}

static addPingCount(builder:flatbuffers.Builder, pingCount:number) {
  builder.addFieldInt32(2, pingCount, 0);
}

static addModeIndex(builder:flatbuffers.Builder, modeIndexOffset:flatbuffers.Offset) {
  builder.addFieldOffset(3, modeIndexOffset, 0);
}

static createModeIndexVector(builder:flatbuffers.Builder, data:number[]|Int32Array):flatbuffers.Offset;
/**
 * @deprecated This Uint8Array overload will be removed in the future.
 */
static createModeIndexVector(builder:flatbuffers.Builder, data:number[]|Uint8Array):flatbuffers.Offset;
static createModeIndexVector(builder:flatbuffers.Builder, data:number[]|Int32Array|Uint8Array):flatbuffers.Offset {
  builder.startVector(4, data.length, 4);
  for (let i = data.length - 1; i >= 0; i--) {
    builder.addInt32(data[i]!);
  }
  return builder.endVector();
}

static startModeIndexVector(builder:flatbuffers.Builder, numElems:number) {
  builder.startVector(4, numElems, 4);
}

static endXsfAcquisitionMode(builder:flatbuffers.Builder):flatbuffers.Offset {
  const offset = builder.endObject();
  return offset;
}

static createXsfAcquisitionMode(builder:flatbuffers.Builder, modeCount:number, modeNameOffset:flatbuffers.Offset, pingCount:number, modeIndexOffset:flatbuffers.Offset):flatbuffers.Offset {
  XsfAcquisitionMode.startXsfAcquisitionMode(builder);
  XsfAcquisitionMode.addModeCount(builder, modeCount);
  XsfAcquisitionMode.addModeName(builder, modeNameOffset);
  XsfAcquisitionMode.addPingCount(builder, pingCount);
  XsfAcquisitionMode.addModeIndex(builder, modeIndexOffset);
  return XsfAcquisitionMode.endXsfAcquisitionMode(builder);
}
}
