// automatically generated by the FlatBuffers compiler, do not modify

import * as flatbuffers from 'flatbuffers';

import { LogLevel } from './log-level.js';


export class LogPayload {
  bb: flatbuffers.ByteBuffer|null = null;
  bb_pos = 0;
  __init(i:number, bb:flatbuffers.ByteBuffer):LogPayload {
  this.bb_pos = i;
  this.bb = bb;
  return this;
}

static getRootAsLogPayload(bb:flatbuffers.ByteBuffer, obj?:LogPayload):LogPayload {
  return (obj || new LogPayload()).__init(bb.readInt32(bb.position()) + bb.position(), bb);
}

static getSizePrefixedRootAsLogPayload(bb:flatbuffers.ByteBuffer, obj?:LogPayload):LogPayload {
  bb.setPosition(bb.position() + flatbuffers.SIZE_PREFIX_LENGTH);
  return (obj || new LogPayload()).__init(bb.readInt32(bb.position()) + bb.position(), bb);
}

level():LogLevel {
  const offset = this.bb!.__offset(this.bb_pos, 4);
  return offset ? this.bb!.readInt32(this.bb_pos + offset) : LogLevel.DEBUG;
}

message():string|null
message(optionalEncoding:flatbuffers.Encoding):string|Uint8Array|null
message(optionalEncoding?:any):string|Uint8Array|null {
  const offset = this.bb!.__offset(this.bb_pos, 6);
  return offset ? this.bb!.__string(this.bb_pos + offset, optionalEncoding) : null;
}

date():string|null
date(optionalEncoding:flatbuffers.Encoding):string|Uint8Array|null
date(optionalEncoding?:any):string|Uint8Array|null {
  const offset = this.bb!.__offset(this.bb_pos, 8);
  return offset ? this.bb!.__string(this.bb_pos + offset, optionalEncoding) : null;
}

stack():string|null
stack(optionalEncoding:flatbuffers.Encoding):string|Uint8Array|null
stack(optionalEncoding?:any):string|Uint8Array|null {
  const offset = this.bb!.__offset(this.bb_pos, 10);
  return offset ? this.bb!.__string(this.bb_pos + offset, optionalEncoding) : null;
}

static startLogPayload(builder:flatbuffers.Builder) {
  builder.startObject(4);
}

static addLevel(builder:flatbuffers.Builder, level:LogLevel) {
  builder.addFieldInt32(0, level, LogLevel.DEBUG);
}

static addMessage(builder:flatbuffers.Builder, messageOffset:flatbuffers.Offset) {
  builder.addFieldOffset(1, messageOffset, 0);
}

static addDate(builder:flatbuffers.Builder, dateOffset:flatbuffers.Offset) {
  builder.addFieldOffset(2, dateOffset, 0);
}

static addStack(builder:flatbuffers.Builder, stackOffset:flatbuffers.Offset) {
  builder.addFieldOffset(3, stackOffset, 0);
}

static endLogPayload(builder:flatbuffers.Builder):flatbuffers.Offset {
  const offset = builder.endObject();
  return offset;
}

static finishLogPayloadBuffer(builder:flatbuffers.Builder, offset:flatbuffers.Offset) {
  builder.finish(offset);
}

static finishSizePrefixedLogPayloadBuffer(builder:flatbuffers.Builder, offset:flatbuffers.Offset) {
  builder.finish(offset, undefined, true);
}

static createLogPayload(builder:flatbuffers.Builder, level:LogLevel, messageOffset:flatbuffers.Offset, dateOffset:flatbuffers.Offset, stackOffset:flatbuffers.Offset):flatbuffers.Offset {
  LogPayload.startLogPayload(builder);
  LogPayload.addLevel(builder, level);
  LogPayload.addMessage(builder, messageOffset);
  LogPayload.addDate(builder, dateOffset);
  LogPayload.addStack(builder, stackOffset);
  return LogPayload.endLogPayload(builder);
}
}
