// automatically generated by the FlatBuffers compiler, do not modify

import * as flatbuffers from 'flatbuffers';

export class XsfAcquisitionModeRequest {
  bb: flatbuffers.ByteBuffer|null = null;
  bb_pos = 0;
  __init(i:number, bb:flatbuffers.ByteBuffer):XsfAcquisitionModeRequest {
  this.bb_pos = i;
  this.bb = bb;
  return this;
}

static getRootAsXsfAcquisitionModeRequest(bb:flatbuffers.ByteBuffer, obj?:XsfAcquisitionModeRequest):XsfAcquisitionModeRequest {
  return (obj || new XsfAcquisitionModeRequest()).__init(bb.readInt32(bb.position()) + bb.position(), bb);
}

static getSizePrefixedRootAsXsfAcquisitionModeRequest(bb:flatbuffers.ByteBuffer, obj?:XsfAcquisitionModeRequest):XsfAcquisitionModeRequest {
  bb.setPosition(bb.position() + flatbuffers.SIZE_PREFIX_LENGTH);
  return (obj || new XsfAcquisitionModeRequest()).__init(bb.readInt32(bb.position()) + bb.position(), bb);
}

filePath():string|null
filePath(optionalEncoding:flatbuffers.Encoding):string|Uint8Array|null
filePath(optionalEncoding?:any):string|Uint8Array|null {
  const offset = this.bb!.__offset(this.bb_pos, 4);
  return offset ? this.bb!.__string(this.bb_pos + offset, optionalEncoding) : null;
}

static startXsfAcquisitionModeRequest(builder:flatbuffers.Builder) {
  builder.startObject(1);
}

static addFilePath(builder:flatbuffers.Builder, filePathOffset:flatbuffers.Offset) {
  builder.addFieldOffset(0, filePathOffset, 0);
}

static endXsfAcquisitionModeRequest(builder:flatbuffers.Builder):flatbuffers.Offset {
  const offset = builder.endObject();
  return offset;
}

static createXsfAcquisitionModeRequest(builder:flatbuffers.Builder, filePathOffset:flatbuffers.Offset):flatbuffers.Offset {
  XsfAcquisitionModeRequest.startXsfAcquisitionModeRequest(builder);
  XsfAcquisitionModeRequest.addFilePath(builder, filePathOffset);
  return XsfAcquisitionModeRequest.endXsfAcquisitionModeRequest(builder);
}
}
