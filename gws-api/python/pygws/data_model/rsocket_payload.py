import logging
import time
import traceback
from typing import Dict, List

import flatbuffers
from rsocket.extensions.helpers import composite, route
from rsocket.payload import Payload

import pygws.data_model.flatbuffers.protocol_list_generated as proto_list
import pygws.data_model.flatbuffers.protocol_log_generated as proto_log
import pygws.data_model.flatbuffers.protocol_map_of_double_generated as proto_map_of_double
import pygws.data_model.flatbuffers.protocol_progress_generated as proto_progress

log_level_to_flatbuffers_level = {
    logging.DEBUG: proto_log.LogLevel.DEBUG,
    logging.INFO: proto_log.LogLevel.INFO,
    logging.WARN: proto_log.LogLevel.WARN,
    logging.ERROR: proto_log.LogLevel.ERROR,
}

TERMINATE_ROUTE = "terminate"
CANCEL_ROUTE = "cancel"


def _make_payload(flatbuffers_builder: flatbuffers.Builder, route_target: str) -> Payload:
    """
    Makes a payload for the specified route
    """
    metadata = composite(route(route_target))
    return Payload(flatbuffers_builder.Output(), metadata)


def make_terminate_payload():
    """Return a terminate payload"""
    return Payload(None, composite(route(TERMINATE_ROUTE)))


def make_cancel_payload():
    """Return a cancel payload"""
    return Payload(None, composite(route(CANCEL_ROUTE)))


def make_log_payload(log_record: logging.LogRecord) -> Payload:
    """
    Makes payload to send log informations
    """
    builder = flatbuffers.Builder()
    message_string = builder.CreateString(log_record.getMessage())

    # Format date/time
    iso_date = time.strftime("%Y-%m-%dT%H:%M:%S", time.localtime(log_record.created))
    date_string = builder.CreateString(iso_date)

    # Format stack
    stack = ""
    if log_record.exc_info:
        stack = "".join(traceback.format_exception(*log_record.exc_info))
    stack_string = builder.CreateString(stack)

    level = proto_log.LogLevel.WARN
    if log_record.levelno in log_level_to_flatbuffers_level:
        level = log_level_to_flatbuffers_level[log_record.levelno]

    proto_log.LogPayloadStart(builder)
    proto_log.LogPayloadAddLevel(builder, level)
    proto_log.LogPayloadAddMessage(builder, message_string)
    proto_log.LogPayloadAddDate(builder, date_string)
    proto_log.LogPayloadAddStack(builder, stack_string)
    result_payload = proto_log.LogPayloadEnd(builder)
    builder.Finish(result_payload)

    return _make_payload(builder, route_target="LogPayload")


def make_files_payload(files: List[str]) -> Payload:
    """
    Makes a payload of list of files
    """
    builder = flatbuffers.Builder()
    file_paths = [builder.CreateString(path) for path in files[::-1]]
    proto_list.FileListPayloadStartFilePathsVector(builder, len(file_paths))
    for file_path in file_paths:
        builder.PrependSOffsetTRelative(file_path)
    files_offset = builder.EndVector(len(file_paths))

    proto_list.FileListPayloadStart(builder)
    proto_list.FileListPayloadAddFilePaths(builder, files_offset)

    result_payload = proto_list.FileListPayloadEnd(builder)
    builder.Finish(result_payload)

    return _make_payload(builder, route_target="FileListPayload")


def make_strings_payload(strings: List[str]) -> Payload:
    """
    Makes a payload of list of strings
    """
    builder = flatbuffers.Builder()
    built_strings = [builder.CreateString(path) for path in strings[::-1]]
    proto_list.StringListPayloadStartStringsVector(builder, len(built_strings))
    for built_string in built_strings:
        builder.PrependSOffsetTRelative(built_string)
    strings_offset = builder.EndVector(len(built_strings))

    proto_list.StringListPayloadStart(builder)
    proto_list.StringListPayloadAddStrings(builder, strings_offset)

    result_payload = proto_list.StringListPayloadEnd(builder)
    builder.Finish(result_payload)

    return _make_payload(builder, route_target="StringListPayload")


def make_progress_payload(task_name: str, progress: float) -> Payload:
    """
    Makes the progression payload.
    progress : progression from 0.0 to 1.0
    """
    builder = flatbuffers.Builder()

    task_string = builder.CreateString(task_name)
    proto_progress.ProgressPayloadStart(builder)
    proto_progress.ProgressPayloadAddTaskname(builder, task_string)
    proto_progress.ProgressPayloadAddProgress(builder, progress)
    result_payload = proto_progress.ProgressPayloadEnd(builder)
    builder.Finish(result_payload)

    return _make_payload(builder, route_target="ProgressPayload")


def make_map_of_float_payload(values: Dict[str, float]) -> Payload:
    """
    Makes a map of float payload (spatial resolution, geobox...)
    """
    builder = flatbuffers.Builder()
    entries = []
    for key, value in values.items():
        str_key = builder.CreateString(key)
        proto_map_of_double.MapOfDoubleEntryStart(builder)
        proto_map_of_double.MapOfDoubleEntryAddKey(builder, str_key)
        proto_map_of_double.MapOfDoubleEntryAddValue(builder, value)
        entries.append(proto_map_of_double.MapOfDoubleEntryEnd(builder))

    proto_map_of_double.MapOfDoublePayloadStartEntriesVector(builder, len(entries))
    for entry in entries:
        builder.PrependSOffsetTRelative(entry)
    entries_offset = builder.EndVector(len(entries))

    proto_map_of_double.MapOfDoublePayloadStart(builder)
    proto_map_of_double.MapOfDoublePayloadAddEntries(builder, entries_offset)
    result_payload = proto_map_of_double.MapOfDoublePayloadEnd(builder)
    builder.Finish(result_payload)

    return _make_payload(builder, route_target="MapOfDoublePayload")
