from typing import Any, List

from pydantic import BaseModel, Field, RootModel


class GwsWizardPage(BaseModel):
    """
    Instance representing a set of arguments grouped in the same wizard page
    """

    # Title of the page
    title: str = Field(default="")
    # List of argument names belonging to this page.
    parameters: List[str] = Field(default_factory=list)
    # Description of the page
    description: str = Field(default="")
    # True when this page groups some technical values, hidden to user
    visible: bool = Field(default=True)


class GwsArgumentConfiguration(BaseModel):
    """
    Description of an argument expected by the execution of the service
    """

    # Name of the argument. This is its id
    name: str = Field(default="")
    # Type. One of :
    #  - bool, int, float, string, datetime, duration
    #  - file, folder, infile, indir, outfile
    #  - projection, geobox#[...]
    #  - layers, checklist, wc_filters
    #  - csv#[...], mask#[...], time_interval#[...], cdi#[...], cdi_filter#[...], sensors
    type: str = Field(default="")
    # CLI key of this arg
    key: str = Field(default="")
    # CLI long key of this arg
    long_key: str = Field(default="")
    # Nb of possible values (+ means one or none)
    nargs: str = Field(default="+")
    # Possible values
    choices: List = Field(default_factory=list)
    # Textual help
    help: str = Field(default="")
    # Default value
    default: Any | None = Field(default=None)
    # Unit of the value
    units: str = Field(default="")
    # Service path to call for evaluating this argument
    function: str = Field(default="")
    # Name of the page in the wizard
    page: str = Field(default="")


class GwsServiceConfiguration(BaseModel):
    """
    Set of arguments.
    Describe all arguments accepted by the service.
    Hold some information to manage the wizard (visibility, grouping in page)
    """

    name: str = Field(default="")
    # List of possible argument to launch the service
    parameters: List[GwsArgumentConfiguration] = Field(default_factory=list)
    # Python function to execute
    function: str = Field(default="")
    # Python script to execute.
    file: str = Field(default="")
    # Path of the html file describing the service
    help: str = Field(default="")
    # False in case of technical service, not visible to users
    visible: bool = Field(default=True)
    # List of wizard pages
    pages: List[GwsWizardPage] = Field(default_factory=list)


class GwsService(BaseModel):
    """
    Service definition
    A service always have an id and a name

    Attributes group_path and configuration_path are only available when the service belonging to the list of service provided by GWS  (http://.../gws/services)
    Attributes description (all arguments loaded) and help are only available when a detailed service has been requested to GWS (http://.../gws/services/{id})
    """

    # Unique id of the service
    id: int
    # Name of the service
    name: str = Field(default="")
    # Parent group path of the service. group_path/name is unique
    group_path: str = Field(default="")
    # Path to the json file describing the arguments. May be referenced by GwsArgumentConf.function
    configuration_path: str = Field(default="")

    # Set of arguments
    description: GwsServiceConfiguration | None = Field(default=None)
    # HTML help message (contain of GwsServiceConf.help)
    help: str = Field(default="")


# Convenient class to deserialize a json of services with pydantic
GwsServiceList = RootModel[List[GwsService]]


class GwsArgument(BaseModel):
    """
    Instance of argument expected by a service execution
    """

    # Name of the argument. See GwsServiceConfiguration.name
    name: str = Field(default="")
    # Value of the argument according GwsServiceConfiguration.type
    value: Any = Field(default="")
