from datetime import datetime
from typing import List

from pydantic import BaseModel, Field, RootModel


class GwsJob(BaseModel):
    """
    Instance of job representing a service execution.
    """

    # Unique id of the job
    job_id: int = Field(default=0)
    # Id of the executed service
    service_id: int = Field(default=0)
    # Name of the executed service
    service_name: str = Field(default="")
    # Host of the RSocket (transport http websocket). Connect to get info, log, data...
    websocket_host: str = Field(default="")
    # Port of the RSocket (transport http websocket) to connect to
    websocket_port: int = Field(default=0)
    # Status of the job (PENDING, RUNNING, SUCCESS, ERROR, CANCELLED)
    status: str = Field(default="")
    # Dates of life cycle
    create_time: datetime | None = Field(default=None)
    start_time: datetime | None = Field(default=None)
    end_time: datetime | None = Field(default=None)


# Convenient class to deserialize a json of jobs with pydantic
GwsJobList = RootModel[List[GwsJob]]
