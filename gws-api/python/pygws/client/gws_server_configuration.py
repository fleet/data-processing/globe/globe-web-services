from contextvars import ContextVar

__gws_http_host: ContextVar[str] = ContextVar("gws_http_host")
__gws_http_port: ContextVar[int] = ContextVar("gws_http_port")


def configure_gws(gws_http_host: str = "localhost", gws_http_port: int = 8081) -> None:
    """
    Initialization of all context variables
    """
    __gws_http_host.set(gws_http_host)
    __gws_http_port.set(gws_http_port)


def get_gws_http_base_url() -> str:
    """Return the url of GWS"""
    return f"http://{__gws_http_host.get()}:{__gws_http_port.get()}/gws"

def get_gws_host() -> str:
    return __gws_http_host.get()