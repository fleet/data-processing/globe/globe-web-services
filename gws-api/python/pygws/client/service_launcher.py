import asyncio
from abc import ABC, abstractmethod
from pathlib import Path
from typing import Dict, List

from result import Err, Ok, Result

import pygws.client.gws_server_proxy as gws_proxy
from pygws.client.gws_error import GwsError
from pygws.client.gws_logger import logger
from pygws.client.payload_interpreters import (
    DictOfFloatPayloadInterpreter,
    FileListPayloadInterpreter,
    LogPayloadInterpreter,
    PayloadInterpreter,
)
from pygws.client.websocket_client import GwsWebSocketClient
from pygws.data_model.job_data import GwsJob
from pygws.data_model.service_data import GwsArgument, GwsService
import pygws.client.gws_server_configuration as gws_conf


class ServiceLauncher(ABC):
    """
    Abstract class defining how to search and run a Gws service
    """

    def __init__(self, service_name: str, payload_interpreters: List[PayloadInterpreter]):
        """
        Constructor
        """
        self._service_name = service_name
        self._payload_interpreters = payload_interpreters
        self._service_client: GwsWebSocketClient | None = None

    @abstractmethod
    def init_arguments(self, service: GwsService) -> List[GwsArgument]:
        """
        Service has been found and the configuration is available.
        According to the configuration, builds and returns the list of arguments to run the service
        """

    async def _run_service(self) -> GwsJob:
        """
        Search and run a Gws service.
        Return the GwsJob representing the running service of GWS or None on error
        """
        logger.debug("Running service '%s' requested", self._service_name)

        try:
            service = await gws_proxy.get_service_by_name(self._service_name)
            arguments = self.init_arguments(service)
            job = await gws_proxy.launch_service(
                service_id=service.id,
                arguments=arguments,
                record_in_log_book=False,
            )
            logger.debug("Service '%s' is running. Job id is %d", self._service_name, job.job_id)
            return job
        except GwsError:
            logger.warning("GWS error occured. Service '%s' not running", self._service_name)
            raise

    async def run(self, cancel_flag: asyncio.Event | None = None) -> Result[GwsJob, BaseException]:
        """
        Launch the service.
        Set up a GwsWebSocketClient to monitor the execution
        Observes cancel_flag. When set, cancel is requested of client
        """

        # Spawn a Task to wait until 'cancel_flag' is set.
        cancel_task = None
        if cancel_flag is not None:

            async def _wait_for_cancel_request():
                """Waits for the cancellation"""
                try:
                    await cancel_flag.wait()
                    await self.cancel()
                except asyncio.CancelledError:
                    pass  # Cancel the cancel at the end of the service

            cancel_task = asyncio.create_task(_wait_for_cancel_request())

        try:
            result = await self._run_service()
            self._service_client = GwsWebSocketClient(gws_conf.get_gws_host(), result.websocket_port)
            await self._service_client.run(self._payload_interpreters)
            self._service_client = None
            return Ok(result)
        except GwsError as gex:
            return Err(gex)
        except Exception as exc:
            logger.exception("Service failed")
            return Err(exc)
        finally:
            # Stop the cancel task
            if cancel_task is not None:
                cancel_task.cancel()
                await cancel_task

    async def cancel(self):
        """Managing cancel event"""
        logger.debug("Cancel requested. Informing the service '%s'", self._service_name)
        if self._service_client is not None:
            await self._service_client.cancel()


async def run_service_and_return_output_files(
    service_name: str, arguments: Dict, cancel_flag: asyncio.Event | None = None
) -> Result[List[Path], BaseException]:
    """
    Builds a service and run it.
    Returns the list of intercepted files
    """
    logger.info("Preparing GWS service '%s'", service_name)

    files_intercepter = FileListPayloadInterpreter()
    log_payload_interpreter = LogPayloadInterpreter()

    class FileListServiceLauncher(ServiceLauncher):
        """
        Launch a service returning a list of file
        """

        def __init__(self):
            super().__init__(
                service_name,
                payload_interpreters=[log_payload_interpreter, files_intercepter],
            )

        def init_arguments(self, service: GwsService) -> List[GwsArgument]:
            return [GwsArgument(name=argument_name, value=value) for argument_name, value in arguments.items()]

    result = await FileListServiceLauncher().run(cancel_flag)
    if result.is_err():
        return result

    if log_payload_interpreter.has_errors:
        logger.debug("Some error messages were received from the service. Considered as failed")
        return Err(GwsError("Service failed"))

    return Ok(files_intercepter.intercepted_files)


async def run_service_and_return_floats(
    service_name: str, arguments: Dict, cancel_flag: asyncio.Event | None = None
) -> Result[Dict[str, float], Exception]:
    """
    Builds a service and run it.
    Returns the dict of intercepted floats
    """
    logger.info("Preparing GWS service '%s'", service_name)

    floats_intercepter = DictOfFloatPayloadInterpreter()
    log_payload_interpreter = LogPayloadInterpreter()

    class DictFloatServiceLauncher(ServiceLauncher):
        """
        Launch a service returning a dict of floats
        """

        def __init__(self):
            super().__init__(
                service_name,
                payload_interpreters=[log_payload_interpreter, floats_intercepter],
            )

        def init_arguments(self, service: GwsService) -> List[GwsArgument]:
            return [GwsArgument(name=argument_name, value=value) for argument_name, value in arguments.items()]

    result = await DictFloatServiceLauncher().run(cancel_flag)
    if result.is_err():
        return result

    if log_payload_interpreter.has_errors:
        logger.debug("Some error messages were received from the service. Considered as failed")
        return Err(GwsError("Service failed"))
    return Ok(floats_intercepter.intercepted_floats)
