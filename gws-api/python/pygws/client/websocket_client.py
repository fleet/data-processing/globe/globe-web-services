import asyncio

from rsocket.transports.aiohttp_websocket import TransportAioHttpClient
from rsocket.transports.transport import Transport

from pygws.client.gws_logger import logger
from pygws.client.rsocket_client import GwsSocketClient, wait_for_connection_closed


class GwsTransportAioHttpClient(TransportAioHttpClient):
    """
    Redefinition of TransportAioHttpClient to catch the moment when sending a frame failed : connection is lost
    """

    def __init__(self, url, terminate_event: asyncio.Event, connection_lost_event: asyncio.Event):
        super().__init__(url=url)
        self._terminate_event = terminate_event
        self._connection_lost_event = connection_lost_event

    async def send_frame(self, frame):
        try:
            if not self._connection_lost_event.is_set() and not self._terminate_event.is_set():
                await super().send_frame(frame)
        except:  # pylint: disable=bare-except
            logger.error("Communication error with GWS. Connection closed")
            self._connection_lost_event.set()


class GwsWebSocketClient(GwsSocketClient):
    """
    Client side instance of an GWS web socket connection
    """

    def __init__(
        self,
        host: str,
        port: int,
        process_with_rsocket=wait_for_connection_closed,
    ):
        """
        Constructor.
        """
        super().__init__(host, port, process_with_rsocket)

    async def _make_transport(self, terminate_event: asyncio.Event, connection_lost_event: asyncio.Event) -> Transport:
        """Creates the instance of Transport"""
        socket_url = f"http://{self.host}:{self.port}"
        logger.debug("Connection to %s", socket_url)
        return GwsTransportAioHttpClient(socket_url, terminate_event, connection_lost_event)
