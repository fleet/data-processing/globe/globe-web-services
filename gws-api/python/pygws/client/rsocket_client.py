import asyncio
from abc import ABC, abstractmethod
from datetime import timedelta
from typing import List

from rsocket.extensions.mimetypes import WellKnownMimeTypes
from rsocket.helpers import single_transport_provider
from rsocket.rsocket_client import RSocketClient
from rsocket.transports.transport import Transport

from pygws.client.gws_error import GwsError
from pygws.client.gws_logger import logger as default_logger
from pygws.client.payload_interpreters import PayloadInterpreter
from pygws.client.payload_request_handler import PayloadHandler
from pygws.data_model.rsocket_payload import make_cancel_payload


class GwsSocketClient(ABC):
    """
    Client side instance of an GWS connection
    """

    def __init__(self, host: str, port: int, process_with_rsocket):
        """
        Constructor.
        process_with_rsocket : what to do when the connection is established
        """
        self.host = host
        self.port = port
        self._process_with_rsocket = process_with_rsocket
        # Client side instance of an RSocket connection
        self.rsocket_client: RSocketClient | None = None

    @abstractmethod
    async def _make_transport(self, terminate_event: asyncio.Event, connection_lost_event: asyncio.Event) -> Transport:
        """Returns the instance of Transport"""

    async def run(self, payload_interpreters: List[PayloadInterpreter]):
        """
        Establish the communication with GWS.
        Wait until the communication is lost
        """
        payload_router = PayloadHandler(payload_interpreters=payload_interpreters)

        def _get_payload_router():
            return payload_router

        transport = await self._make_transport(payload_router.terminate_event, payload_router.connection_lost_event)
        try:
            default_logger.debug("Opening the socket")
            self.rsocket_client = RSocketClient(
                transport_provider=single_transport_provider(transport),
                handler_factory=_get_payload_router,
                keep_alive_period=timedelta(seconds=2),
                data_encoding=WellKnownMimeTypes.APPLICATION_CBOR,
                metadata_encoding=WellKnownMimeTypes.MESSAGE_RSOCKET_COMPOSITE_METADATA,
            )
            await self.rsocket_client.connect()
            if payload_router.error_occured is None:
                await self._process_with_rsocket(
                    self.rsocket_client, payload_router.terminate_event, payload_router.connection_lost_event
                )
                default_logger.debug("Connection with GWS closed")

        except GwsError:
            raise
        except Exception as ex:
            default_logger.exception("An error occured")
            raise GwsError(f"Error during service execution :{str(ex)}") from ex
        finally:
            if self.rsocket_client:
                # _is_server_alive set to False to stop _keepalive_timeout_task and allow cancelling the task
                self.rsocket_client._is_server_alive = False  # pylint: disable=protected-access
                await self.rsocket_client.close()
        if payload_router.error_occured is not None:
            raise GwsError(payload_router.error_occured)

    async def cancel(self) -> None:
        """
        Send a cancel request to the correspondent
        """
        if self.rsocket_client is not None:
            await self.rsocket_client.fire_and_forget(make_cancel_payload())
            default_logger.debug("Cancel message sent")


async def wait_for_connection_closed(
    client: RSocketClient, terminate_event: asyncio.Event, connection_lost_event: asyncio.Event
):
    """A Web socket client just waits for the end of the connection.
    All the communication is managed by the PayloadInterpreter(s)"""
    while client.is_server_alive() and not terminate_event.is_set() and not connection_lost_event.is_set():
        await asyncio.sleep(0.5)
