from typing import List

import httpx

import pygws.client.gws_server_configuration as gws_conf
from pygws.client.gws_error import GwsError
from pygws.client.gws_logger import logger
from pygws.data_model.job_data import GwsJob, GwsJobList
from pygws.data_model.service_data import GwsArgument, GwsService, GwsServiceList


def _manage_http_error(response) -> GwsError:
    message = f"Request to GWS failed with the HTTP error {response.status_code}"
    logger.warning(message)
    answer = response.text
    if answer:
        logger.warning(answer)
    return GwsError(message)


async def get_all_services() -> List[GwsService]:
    """
    Request the list of available service to GWS server
    Return a list of GwsService
    """
    logger.debug("Getting all services from GWS")
    try:
        async with httpx.AsyncClient(timeout=None) as client:
            response = await client.get(f"{gws_conf.get_gws_http_base_url()}/services?format=list")
            if response.is_success:
                return GwsServiceList.model_validate_json(response.text).root
            raise _manage_http_error(response)
    except GwsError:
        raise
    except httpx.ConnectError as conn_ex:
        logger.warning("GWS not reachable")
        raise GwsError("GWS not reachable") from conn_ex
    except Exception as exc:
        logger.exception("Unable to get services from GWS")
        raise GwsError("Unable to get services from GWS") from exc


async def get_service(service_id: int) -> GwsService:
    """
    Request the description of a service to GWS server
    """
    logger.debug("Getting service %d from GWS", service_id)

    try:
        async with httpx.AsyncClient(timeout=None) as client:
            response = await client.get(f"{gws_conf.get_gws_http_base_url()}/services/{service_id}")
            if response.is_success:
                return GwsService.model_validate_json(response.text)
            raise _manage_http_error(response)
    except GwsError:
        raise
    except httpx.ConnectError as conn_ex:
        logger.warning("GWS not reachable")
        raise GwsError("GWS not reachable") from conn_ex
    except Exception as exc:
        logger.exception("Unable to get services from GWS")
        raise GwsError("Unable to get services from GWS") from exc


async def get_service_by_name(service_name: str) -> GwsService:
    """
    Request the description of a first service with the specified name
    """
    logger.debug("Getting service '%s' from GWS", service_name)

    try:
        all_services = await get_all_services()
        service = next(filter(lambda s: s.name == service_name, all_services))

        # Get the argument description of the service
        service = await get_service(service.id)

        # Sanity check
        if service is not None and service.description is not None and service.description.parameters is not None:
            return service
    except StopIteration:
        logger.warning("Service %s not found", service_name)
    except GwsError:
        raise
    except Exception as exc:
        logger.exception("Unable to get services from GWS")
        raise GwsError("Unable to get services from GWS") from exc

    # Service not found
    raise GwsError(f"Service '{service_name}' not found")


async def launch_service(service_id: int, arguments: List[GwsArgument], record_in_log_book: bool = False) -> GwsJob:
    """
    Request a service execution. Service is identified by its id.
    Arguments are serialized in a JSON format and send as data to the post request
    When record_in_log_book is True, the execution is recorded in the log book

    returns the GwsJob to manage the service execution.
    """
    logger.debug("Launching service '%d'", service_id)

    payload = {}
    for arg in arguments:
        payload[arg.name] = arg.value
    logger.debug("Arguments are %s", payload)

    try:
        async with httpx.AsyncClient(timeout=None) as client:
            response = await client.post(
                url=f"{gws_conf.get_gws_http_base_url()}/services/{service_id}?logbook={'true' if record_in_log_book else 'false'}",
                json=payload,
            )
            if response.is_success:
                return GwsJob.model_validate_json(response.text)
            raise _manage_http_error(response)
    except GwsError:
        raise
    except httpx.ConnectError as conn_ex:
        logger.warning("GWS not reachable")
        raise GwsError("GWS not reachable") from conn_ex
    except Exception as exc:
        logger.exception("Unable to get the service from GWS")
        raise GwsError("Unable to get the service from GWS") from exc


async def get_all_jobs() -> List[GwsJob]:
    """
    Request the list of jobs to GWS server
    Return a list of GwsJob
    """
    logger.debug("Getting all jobs from GWS")

    try:
        async with httpx.AsyncClient(timeout=None) as client:
            response = await client.get(f"{gws_conf.get_gws_http_base_url()}/jobs")
            if response.is_success:
                return GwsJobList.model_validate_json(response.text).root
            raise _manage_http_error(response)
    except GwsError:
        raise
    except Exception as exc:
        logger.exception("Unable to get jobs from GWS")
        raise GwsError("Unable to get jobs from GWS") from exc


async def get_job(job_id: int) -> GwsJob:
    """
    Request the description of a job to GWS server
    """
    logger.debug("Getting job %d from GWS", job_id)

    try:
        async with httpx.AsyncClient(timeout=None) as client:
            response = await client.get(f"{gws_conf.get_gws_http_base_url()}/jobs/{job_id}")
            if response.is_success:
                return GwsJob.model_validate_json(response.text)
            raise _manage_http_error(response)
    except GwsError:
        raise

    except httpx.ConnectError as conn_ex:
        logger.warning("GWS not reachable")
        raise GwsError("GWS not reachable") from conn_ex
    except Exception as exc:
        logger.exception("Unable to get the job from GWS")
        raise GwsError("Unable to get the job from GWS") from exc
