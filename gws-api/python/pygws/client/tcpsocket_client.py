import asyncio

from rsocket.transports.tcp import TransportTCP
from rsocket.transports.transport import Transport

from pygws.client.rsocket_client import GwsSocketClient, wait_for_connection_closed


class GwsTcpSocketClient(GwsSocketClient):
    """
    Client side instance of an GWS tcp socket connection
    This class is abstract. Subclasses must implement _on_connected to perform the service
    """

    def __init__(
        self,
        host: str,
        port: int,
        process_with_rsocket=wait_for_connection_closed,
    ):
        """
        Constructor.
        """
        super().__init__(host, port, process_with_rsocket)

    async def _make_transport(self, terminate_event: asyncio.Event, connection_lost_event: asyncio.Event) -> Transport:
        """Creates the instance of Transport"""
        reader, writer = await asyncio.open_connection(self.host, self.port)
        return TransportTCP(reader, writer)
