import asyncio
from datetime import timedelta
from typing import Awaitable, List, Optional

from rsocket.extensions.composite_metadata import CompositeMetadata
from rsocket.extensions.helpers import require_route
from rsocket.helpers import create_response
from rsocket.payload import Payload
from rsocket.request_handler import BaseRequestHandler

from pygws.client.gws_logger import logger
from pygws.client.payload_interpreters import (
    CancelPayloadInterpreter,
    PayloadInterpreter,
)
from pygws.data_model.rsocket_payload import CANCEL_ROUTE, TERMINATE_ROUTE


class PayloadHandler(BaseRequestHandler):
    """
    Instance of RequestHandler required by RSocketClient to manage the Payload received by fire_and_forget request.
    """

    def __init__(self, payload_interpreters: List[PayloadInterpreter]):
        self.payload_interpreters = {
            interpreter.get_rsocket_route(): interpreter for interpreter in payload_interpreters
        }

        # Set a cancel interpreter by default if not provided
        if CANCEL_ROUTE not in self.payload_interpreters:
            self.payload_interpreters[CANCEL_ROUTE] = CancelPayloadInterpreter()

        # True when terminate signal is received from the correspondent
        self.terminate_event = asyncio.Event()
        # Message of the last occured error.
        self.error_occured: str | None = None
        # Set to true when socket has been closed suddenly
        self.connection_lost_event = asyncio.Event()

    async def request_fire_and_forget(self, payload: Payload):
        """
        Extracts the route contained in the metadata and call the interpreter in charge of it
        """
        composite_metadata = CompositeMetadata()
        composite_metadata.parse(payload.metadata)
        payload_route = require_route(composite_metadata)
        if payload_route == TERMINATE_ROUTE:
            logger.debug("Terminate payload received")
            self.terminate_event.set()
        elif payload_route in self.payload_interpreters:
            logger.debug("Processing payload on route '%s'", payload_route)
            try:
                await self.payload_interpreters[payload_route].process_payload(payload.data)
            except BaseException as exc:  # pylint: disable=bare-except
                self.error_occured = f"Error while processing payload of route '{payload_route}' : {str(exc)}"
                logger.exception(self.error_occured)

        else:
            logger.debug("Payload with route '%s' not managed", payload_route)

    async def request_response(self, payload: Payload) -> Awaitable[Payload]:
        """
        Extracts the route contained in the metadata and call the interpreter in charge of it
        """
        composite_metadata = CompositeMetadata()
        composite_metadata.parse(payload.metadata)
        payload_route = require_route(composite_metadata)
        if payload_route == TERMINATE_ROUTE:
            logger.debug("Terminate payload received")
            self.terminate_event.set()
        elif payload_route in self.payload_interpreters:
            logger.debug("Processing payload on route '%s'", payload_route)
            try:
                response = await self.payload_interpreters[payload_route].process_payload(payload.data)
                return create_response(response)
            except BaseException as exc:  # pylint: disable=bare-except
                self.error_occured = f"Error while processing payload of route '{payload_route}' : {str(exc)}"
                logger.exception(self.error_occured)
        else:
            logger.debug("Payload with route '%s' not managed", payload_route)

        return create_response()

    async def on_setup(self, data_encoding: bytes, metadata_encoding: bytes, payload: Payload):
        logger.debug("Communication established with GWS")
        await super().on_setup(data_encoding, metadata_encoding, payload)

    async def on_error(self, error_code, payload: Payload):
        self.error_occured = f"Communication error with GWS ({payload.data.decode()}, error code is {error_code.name})"
        self.connection_lost_event.set()
        await super().on_error(error_code, payload)

    async def on_connection_error(self, rsocket, exception: Exception):
        self.error_occured = f"Unable to connect to GWS ({str(exception)})"
        logger.warning(self.error_occured)
        self.connection_lost_event.set()
        await super().on_connection_error(rsocket, exception)

    async def on_keepalive_timeout(self, time_since_last_keepalive: timedelta, rsocket):
        self.error_occured = f"GWS is not alive (timeout after {str(timedelta)})"
        logger.warning(self.error_occured)
        self.connection_lost_event.set()
        await super().on_keepalive_timeout(time_since_last_keepalive, rsocket)

    async def on_close(self, rsocket, exception: Optional[Exception] = None):
        logger.warning(self.error_occured)
        self.connection_lost_event.set()
        await super().on_close(rsocket, exception)
