import logging
from abc import ABC, abstractmethod
from pathlib import Path
from typing import Dict, List, Protocol

from rsocket.helpers import utf8_decode
from rsocket.payload import Payload

from pygws.client.gws_logger import logger as default_logger
from pygws.data_model.flatbuffers.protocol_list_generated import (
    FileListPayload,
    StringListPayload,
)
from pygws.data_model.flatbuffers.protocol_log_generated import LogLevel, LogPayload
from pygws.data_model.flatbuffers.protocol_map_of_double_generated import (
    MapOfDoublePayload,
)
from pygws.data_model.flatbuffers.protocol_progress_generated import ProgressPayload
from pygws.data_model.rsocket_payload import CANCEL_ROUTE


class PayloadInterpreter(Protocol):
    """
    Prococol of all interpreters of rsocket payload.
    Interpreter indicates on which route it expects the payload and have a method to process it
    """

    def get_rsocket_route(self) -> str:
        """Route of payload"""

    async def process_payload(self, payload: bytearray) -> Payload | None:
        """Managing a received payload"""


class CancelPayloadInterpreter:
    """
    React when cancel is received on LogPayload route
    """

    def get_rsocket_route(self) -> str:
        """Route of payload"""
        return CANCEL_ROUTE

    async def process_payload(self, payload: bytearray) -> Payload | None:  # pylint: disable=unused-argument
        """Managing the cancel"""
        default_logger.warning("Cancel requested but not managed")
        return None


class LogPayloadInterpreter:
    """
    Interpreter of payload received on LogPayload route
    """

    def __init__(self, logger: logging.Logger = default_logger):
        self.logger = logger
        self._has_errors = False

    @property
    def has_errors(self):
        """True if an error was received"""
        return self._has_errors

    def get_rsocket_route(self) -> str:
        """Route of payload"""
        return "LogPayload"

    async def process_payload(self, payload: bytearray) -> Payload | None:
        """
        Handling the reception of a LogPayload
        """
        log_payload = LogPayload.GetRootAs(payload)
        level = log_payload.Level()
        message = utf8_decode(log_payload.Message())
        date = utf8_decode(log_payload.Date())
        stack = utf8_decode(log_payload.Stack())
        self.process_log(level=level, date=date, message=message, stack=stack)
        return None

    def process_log(self, level: int, date: str, message: str, stack: str) -> None:  # pylint: disable=unused-argument
        """
        Defines how to process the content of a LogPayload.
        """
        if level == LogLevel.DEBUG:
            self.logger.debug(message)
        elif level == LogLevel.INFO:
            self.logger.info(message)
        elif level == LogLevel.WARN:
            self.logger.warning(message)
        else:
            self._has_errors = True
            self.logger.error(message)
            if stack:
                self.logger.error(stack)


class ProgressPayloadInterpreter:
    """
    Interpreter of payload received on ProgressPayload route
    """

    def __init__(self, logger: logging.Logger = default_logger):
        self.logger = logger

    def get_rsocket_route(self) -> str:
        """Route of payload"""
        return "ProgressPayload"

    async def process_payload(self, payload: bytearray) -> Payload | None:
        """
        Handling the reception of a ProgressPayload
        """
        progress_payload = ProgressPayload.GetRootAs(payload)
        task_name = progress_payload.Taskname()
        self.process_progress(utf8_decode(task_name) if task_name else "", progress_payload.Progress())
        return None

    def process_progress(self, task_name: str, progess: float) -> None:
        """
        Defines how to process the content of a ProgressPayload.
        """
        if task_name:
            self.logger.debug("Progression of '%s' : %d", task_name, int(progess * 100))
        else:
            self.logger.debug("Progression : %d", int(progess * 100))


class DictOfFloatPayloadInterpreter:
    """
    Interpreter of payload received on MapOfDoublePayload route
    """

    def __init__(self):
        self.intercepted_floats: Dict[str, float] = {}

    def get_rsocket_route(self) -> str:
        """Route of payload"""
        return "MapOfDoublePayload"

    async def process_payload(self, payload: bytearray) -> Payload | None:
        """
        Handling the reception of a MapOfDoublePayload
        """
        values = {}
        map_payload = MapOfDoublePayload.GetRootAs(payload)
        for i in range(map_payload.EntriesLength()):
            entry = map_payload.Entries(i)
            values[utf8_decode(entry.Key())] = entry.Value()
        self.process_dict(values)
        return None

    def process_dict(self, values: Dict[str, float]) -> None:
        """
        Defines how to process the content of a MapOfDoublePayload.
        """
        self.intercepted_floats.update(values)


class FileListPayloadInterpreter:
    """
    Interpreter of payload received on FileListPayload route
    """

    def __init__(self):
        self.intercepted_files: List[Path] = []

    def get_rsocket_route(self) -> str:
        """Route of payload"""
        return "FileListPayload"

    async def process_payload(self, payload: bytearray) -> Payload | None:
        """
        Handling the reception of a FileListPayload
        """
        list_payload = FileListPayload.GetRootAs(payload)
        file_list = []
        for i in range(list_payload.FilePathsLength()):
            file_list.append(utf8_decode(list_payload.FilePaths(i)))
        self.process_files(file_list)
        return None

    def process_files(self, values: List[str]) -> None:
        """
        Defines how to process the content of a FileListPayload.
        """
        for value in values:
            self.intercepted_files.append(Path(value))


class StringListPayloadInterpreter(ABC):
    """
    Interpreter of payload received on StringListPayload route
    """

    def get_rsocket_route(self) -> str:
        """Route of payload"""
        return "StringListPayload"

    async def process_payload(self, payload: bytearray) -> Payload | None:
        """
        Handling the reception of a StringListPayload
        """
        list_payload = StringListPayload.GetRootAs(payload)
        string_list = []
        for i in range(list_payload.StringsLength()):
            string_list.append(utf8_decode(list_payload.Strings(i)))
        self.process_strings(string_list)
        return None

    @abstractmethod
    def process_strings(self, strings: List[str]) -> None:
        """
        Defines how to process the content of a StringListPayload.
        """
