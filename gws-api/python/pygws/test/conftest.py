import logging

import pytest

import pygws.client.gws_server_configuration as gws_conf

# Import fixtures to make them available for all tests
pytest_plugins = [
    "pygws.test.fixture.rsocket_server_fixture",
    "pygws.test.fixture.gws_http_fixture",
]


@pytest.fixture(scope="session", autouse=True)
def init_conf_fixture():
    logging.basicConfig(level=logging.INFO)
    # Set up GWS configuration for all tests
    gws_conf.configure_gws(gws_http_port=7777)
