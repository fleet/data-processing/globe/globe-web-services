import asyncio
import multiprocessing as mp
import signal
from functools import partial
from typing import Callable
import time

import pytest
from aiohttp import web
from rsocket.payload import Payload
from rsocket.request_handler import BaseRequestHandler
from rsocket.rsocket_server import RSocketServer
from rsocket.transports.aiohttp_websocket import websocket_handler_factory
from rsocket.transports.tcp import TransportTCP

from pygws.client.payload_interpreters import PayloadInterpreter
from pygws.client.payload_request_handler import PayloadHandler


class SenderHandler(BaseRequestHandler):
    """
    Handler of RSocket requests
    """

    def __init__(self, send_payload_to_client: Callable[[RSocketServer], None]):
        self.send_payload_to_client = send_payload_to_client

    # RSocket server created when first request is done to the web app
    rsocket_server: RSocketServer = None

    async def _send_message_to_client(self):
        """
        RSocket server and client are ready. Starts the communication
        """
        await self.send_payload_to_client(self.rsocket_server)
        signal.raise_signal(signal.SIGINT)

    async def on_setup(self, data_encoding: bytes, metadata_encoding: bytes, payload: Payload):
        """
        Client is connected. Triggers the messages sending
        """
        await asyncio.create_task(self._send_message_to_client())


def _on_server_create(handler: SenderHandler, rsocket_server: RSocketServer):
    """
    Invoked when first request is done to the web app.
    Just grab the RSocket server and send it to handler
    """
    handler.rsocket_server = rsocket_server


def _run_rsocket_server_on_websocket(port: int, send_payload_to_client: Callable[[RSocketServer], None]):
    handler = SenderHandler(send_payload_to_client)

    def get_handler():
        return handler

    try:
        # Initialize and run the HTTP server (stops on signal.SIGINT)
        app = web.Application()
        app.add_routes(
            [
                web.get(
                    "/",
                    websocket_handler_factory(
                        on_server_create=partial(_on_server_create, handler), handler_factory=get_handler
                    ),
                )
            ]
        )
        web.run_app(app, port=port)
    except KeyboardInterrupt:
        pass  # signal.SIGINT


@pytest.fixture
def run_rsocket_server_on_websocket():
    """
    Fixture to mock a RSocket server connected to a Websocket.
    """

    def _mock(port: int, send_payload_to_client: Callable[[RSocketServer], None]) -> mp.Process:
        server = mp.Process(target=partial(_run_rsocket_server_on_websocket, port, send_payload_to_client))
        server.start()
        # Let the server starting
        time.sleep(2)
        return server

    return _mock


async def _run_rsocket_server_on_tcpsocket(port: int, payload_interpreter: PayloadInterpreter):
    handler = PayloadHandler([payload_interpreter])

    try:

        def session(*connection):
            RSocketServer(TransportTCP(*connection), handler_factory=lambda: handler)

        server = await asyncio.start_server(session, "localhost", port)

        async with server:
            await server.serve_forever()

    except KeyboardInterrupt:
        pass  # signal.SIGINT


def _async_run_rsocket_server_on_tcpsocket(port: int, payload_interpreter: PayloadInterpreter):
    asyncio.run(_run_rsocket_server_on_tcpsocket(port, payload_interpreter))


@pytest.fixture
def run_rsocket_server_on_tcpsocket():
    """
    Fixture to mock a RSocket server connected to a Tcp socket.
    """

    def _mock(port: int, payload_interpreter: PayloadInterpreter) -> mp.Process:
        server = mp.Process(target=partial(_async_run_rsocket_server_on_tcpsocket, port, payload_interpreter))
        server.start()
        return server

    return _mock
