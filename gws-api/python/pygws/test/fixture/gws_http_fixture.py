import pytest

SERVICE_MODEL = {
    5: {
        "id": 5,
        "name": "Evaluates spatial resolution of sounding files",
        "group_path": "/root/wizard_functions",
        "configuration_path": "evaluate_sounder_spatial_resolution.json",
    },
    6: {
        "id": 6,
        "name": "Evaluates geobox of sounding files",
        "group_path": "/root/wizard_functions",
        "configuration_path": "evaluate_sounder_geobox.json",
    },
    95: {
        "id": 95,
        "name": "Convert sounding file to XSF",
        "configuration_path": "jar:file:xsf_converter.json",
    },
    96: {
        "id": 96,
        "name": "GWS basic service",
        "configuration_path": "jar:file:gws-services.jar!/services/basic_service_example.json",
    },
}

PARAM_MODEL = {
    5: [{"name": "i_paths", "type": "infile(content_type=XSF_NETCDF_4)"}],
    6: [{"name": "i_paths", "type": "infile(content_type=XSF_NETCDF_4)"}],
    95: [
        {"name": "fmt", "type": "string"},
        {"name": "in", "type": "infolder"},
        {"name": "out", "type": "folder"},
    ],
    96: [],
}


@pytest.fixture(scope="function")
def mock_all_services(httpx_mock):
    def _mock(http_port):
        httpx_mock.add_response(
            url=f"http://localhost:{http_port}/gws/services?format=list", json=list(SERVICE_MODEL.values())
        )

    return _mock


@pytest.fixture(scope="function")
def mock_service(httpx_mock):
    def _mock(service_id, http_port):
        service = {
            "id": service_id,
            "description": {"name": SERVICE_MODEL[service_id]["name"], "parameters": PARAM_MODEL[service_id]},
        }

        httpx_mock.add_response(url=f"http://localhost:{http_port}/gws/services/{service_id}", json=service)

    return _mock


@pytest.fixture(scope="function")
def mock_all_jobs(httpx_mock):
    all_jobs = [
        {
            "job_id": 2,
            "service_id": 95,
            "service_name": "Convert sounding file to XSF",
            "websocket_host": "127.0.0.1",
            "websocket_port": 62318,
            "status": "SUCCESS",
        },
        {
            "job_id": 1,
            "service_id": 1,
            "service_name": "Driver of ADCP files",
            "websocket_host": "127.0.0.1",
            "websocket_port": 62264,
            "status": "RUNNING",
        },
    ]

    def _mock(http_port):
        httpx_mock.add_response(url=f"http://localhost:{http_port}/gws/jobs", json=all_jobs)

    return _mock


@pytest.fixture(scope="function")
def mock_job(httpx_mock):
    def _mock(job_id, service_id, websocket_port, http_port):
        job = {
            "job_id": job_id,
            "service_id": service_id,
            "service_name": SERVICE_MODEL[service_id]["name"],
            "websocket_host": "127.0.0.1",
            "websocket_port": websocket_port,
            "status": "SUCCESS",
        }
        httpx_mock.add_response(url=f"http://localhost:{http_port}/gws/jobs/{job_id}", json=job)

    return _mock


@pytest.fixture(scope="function")
def mock_launch_service(httpx_mock):
    def _mock(service_id, websocket_port, http_port):
        httpx_mock.add_response(
            url=f"http://localhost:{http_port}/gws/services/{service_id}?logbook=false",
            method="POST",
            json={
                "websocket_host": "localhost",
                "websocket_port": websocket_port,
            },  # Response
        )

    return _mock
