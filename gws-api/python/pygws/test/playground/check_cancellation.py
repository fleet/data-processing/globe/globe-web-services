import asyncio
import logging
from typing import List

from pygws.client.payload_interpreters import (
    LogPayloadInterpreter,
    ProgressPayloadInterpreter,
)
from pygws.client.service_launcher import ServiceLauncher
from pygws.data_model.service_data import GwsArgument, GwsService
from pygws.playground.commons import run_launcher


class CancelServiceLauncher(ServiceLauncher):
    """
    Launch a service returning a list of String
    """

    def __init__(self):
        super().__init__(
            "Test cancel",
            payload_interpreters=[
                LogPayloadInterpreter(),
                ProgressPayloadInterpreter(),
            ],
        )

    def init_arguments(self, service: GwsService) -> List[GwsArgument]:
        return [GwsArgument(name="nb_seconds", value=15)]


service_to_cancel = CancelServiceLauncher()


async def wait_8_s_and_cancel():
    await asyncio.sleep(8)
    await service_to_cancel.cancel()


async def async_run():
    await asyncio.gather(
        asyncio.create_task(run_launcher(service_to_cancel)),
        asyncio.create_task(wait_8_s_and_cancel()),
    )


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    asyncio.run(async_run())
