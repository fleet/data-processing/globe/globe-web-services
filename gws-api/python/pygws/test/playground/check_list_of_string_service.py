import asyncio
import logging
from typing import List

from pygws.client.payload_interpreters import StringListPayloadInterpreter
from pygws.client.service_launcher import ServiceLauncher
from pygws.data_model.service_data import GwsArgument, GwsService
from pygws.playground.commons import run_launcher

logger = logging.getLogger("StringListServiceLauncher")


class CutLinesInterpreter(StringListPayloadInterpreter):
    def process_strings(self, string: List[str]) -> None:
        logger.debug("Cut linse produced :")
        for cut_line in string:
            logger.debug(cut_line)


class StringListServiceLauncher(ServiceLauncher):
    """
    Launch a service returning a list of String
    """

    def __init__(self):
        super().__init__(
            "Apply a mask to a MBG file and produce the cut lines",
            payload_interpreters=[CutLinesInterpreter()],
        )

    def init_arguments(self, service: GwsService) -> List[GwsArgument]:
        return [
            GwsArgument(name="i_paths", value=[r"d:/temp/sample.mbg"]),
            GwsArgument(name="mask", value=r"d:/temp/sample.kml"),
        ]


asyncio.run(run_launcher(StringListServiceLauncher()))
