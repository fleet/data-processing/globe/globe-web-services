import asyncio
import logging
from typing import List

from pygws.client.payload_interpreters import FileListPayloadInterpreter
from pygws.client.service_launcher import ServiceLauncher
from pygws.data_model.service_data import GwsArgument, GwsService
from pygws.playground.commons import run_launcher

logger = logging.getLogger("FileListServiceLauncher")


class DtmFileInterpreter(FileListPayloadInterpreter):
    def process_files(self, dtm_files: List[str]) -> None:
        logger.debug("DTM files produced :")
        for dtm_file in dtm_files:
            logger.debug(dtm_file)


class FileListServiceLauncher(ServiceLauncher):
    """
    Launch a service returning a list of String
    """

    def __init__(self):
        super().__init__(
            "Export MBG/XSF to DTM",
            payload_interpreters=[DtmFileInterpreter()],
        )

    def init_arguments(self, service: GwsService) -> List[GwsArgument]:
        return [
            GwsArgument(name="i_paths", value=[r"d:/temp/sample.xsf.nc"]),
            GwsArgument(name="o_paths", value=[r"d:/temp/sample.dtm"]),
            GwsArgument(name="overwrite", value=True),
            GwsArgument(
                value="target_spatial_reference",
                name="+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs",
            ),
            GwsArgument(
                name="coord",
                value={
                    "north": -17.64389017287661,
                    "south": -17.692696028860702,
                    "west": 38.201800781268204,
                    "east": 38.28327886210162,
                },
            ),
            GwsArgument(name="target_resolution", value=3.9e-4),
        ]


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)

    asyncio.run(run_launcher(FileListServiceLauncher()))
