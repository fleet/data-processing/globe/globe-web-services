import asyncio
import logging
import sys
import traceback
from pathlib import Path
from typing import Dict

from result import Result

import pygws.client.gws_server_configuration as gws_conf
import pygws.client.service_launcher as service_launcher

logger = logging.getLogger("WaitingServiceLauncher")


async def _run_service(xsf: Path) -> Result[Dict[str, float], Exception]:
    return await service_launcher.run_service_and_return_floats(
        service_name="Evaluates spatial resolution of sounding files", arguments={"i_paths": [str(xsf)]}
    )


async def _run():
    logging.basicConfig(filename="endurance.log", level=logging.INFO)

    gws_conf.configure_gws()

    input_path = Path("~/test/_work_all_to_dtm/prod").expanduser()

    nb_task = 20
    tasks = []
    for i in range(nb_task):
        tasks.append(asyncio.create_task(_run_service(input_path / "0134_20120607_074733_ShipName.xsf.nc")))

    nb_running_task = 1
    total_done = 0
    while nb_running_task > 0:
        for i in range(nb_task):
            task: asyncio.Task = tasks[i]
            if task.done():
                try:
                    result = task.result()
                    if result.is_ok():
                        dic = result.ok_value
                        if len(dic) == 2:
                            total_done += 1
                            print(f"Tasks {total_done} done")
                            tasks[i] = asyncio.create_task(
                                _run_service(input_path / "0134_20120607_074733_ShipName.xsf.nc")
                            )
                        else:
                            print(result)
                except asyncio.CancelledError:
                    print("Cancelled")
                    pass
                except:
                    logger.exception("Error !")
        nb_running_task = 0
        for i in range(nb_task):
            task: asyncio.Task = tasks[i]
            if not task.done():
                nb_running_task += 1
        await asyncio.sleep(0.5)


if __name__ == "__main__":
    asyncio.run(_run())
