import asyncio
import json
import logging

from httpx import ConnectError

import pygws.client.gws_server_configuration as gws_conf
import pygws.client.gws_server_proxy as gws_proxy

"""
Request the list of services to GWS
Browse each service and parse the JSON configuration provided by GWS
"""


async def _check_one_service() -> None:
    """
    Get the service list.
    Parse all service configurations
    """
    try:
        detailed_service = await gws_proxy.get_service(1)
        if detailed_service.description is not None:
            print(f"{detailed_service.id} : nb of arguments {len(detailed_service.description.parameters)}")
    except:
        logging.getLogger().exception("Bad")


async def _check_all_services() -> None:
    """
    Get the service list.
    Parse all service configurations
    """
    try:
        all_services = await gws_proxy.get_all_services()
        for service in all_services:
            print(f"{service.id} : {service.name}")
            detailed_service = await gws_proxy.get_service(service.id)
            # description never None on a detailed service
            if detailed_service.description is not None:
                print(f"{detailed_service.id} : nb of arguments {len(detailed_service.description.parameters)}")
    except:
        logging.getLogger().exception("Bad")


async def _run():
    # Use default configuration. GWS server is supposed running
    gws_conf.configure_gws()
    try:
        # await _check_one_service()
        await _check_all_services()
    except ConnectError as e:
        print("Unable to connect to GWS. Check the configuration (configure_gws)")
    except json.JSONDecodeError as e:
        print("Unexpected GWS response)")
    except Exception as e:
        print(f"Exception not managed : {type(e)}")


if __name__ == "__main__":
    asyncio.run(_run())
