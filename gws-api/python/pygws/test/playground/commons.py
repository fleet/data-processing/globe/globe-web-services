import logging

import pygws.client.gws_server_configuration as gws_conf
from pygws.client.service_launcher import ServiceLauncher


async def run_launcher(service_launcher: ServiceLauncher):
    """
    Demontrates how to execute a ServiceLauncher
    """
    logging.basicConfig(level=logging.INFO)
    try:
        gws_conf.configure_gws()
        await service_launcher.run()
    except Exception as e:
        print("Failed with an exception: %s" % e)
