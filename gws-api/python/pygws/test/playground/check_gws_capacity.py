import asyncio
import logging
from typing import List

import pygws.client.gws_server_configuration as gws_conf
from pygws.client.gws_server_proxy import get_all_jobs
from pygws.client.payload_interpreters import LogPayloadInterpreter
from pygws.client.service_launcher import ServiceLauncher
from pygws.data_model.service_data import GwsArgument, GwsService

logger = logging.getLogger("WaitingServiceLauncher")


class WaitingServiceLauncher(ServiceLauncher):
    """
    Launch a service returning a list of String
    """

    def __init__(self):
        super().__init__(
            "Basic cancellable Java service",
            payload_interpreters=[LogPayloadInterpreter()],
        )

    def init_arguments(self, service: GwsService) -> List[GwsArgument]:
        return [GwsArgument(name="delay", value=30)]


async def _run_one_service():
    await WaitingServiceLauncher().run()


async def _print_job_stats():
    all_jobs = await get_all_jobs()
    running_jobs = 0
    pending_jobs = 0
    total_jobs = len(all_jobs)
    for job in await get_all_jobs():
        if job.status == "PENDING":
            pending_jobs += 1
        if job.status == "RUNNING":
            running_jobs += 1
    print(f"GWS activity : running/pending/total -> {running_jobs} / {pending_jobs} / {total_jobs} ")
    return running_jobs, pending_jobs, total_jobs


async def _run():
    logging.basicConfig(level=logging.INFO)

    gws_conf.configure_gws()

    background_tasks = []
    for i in range(100):
        one_task = asyncio.get_running_loop().create_task(_run_one_service())
        background_tasks.append(one_task)
        print(f"Service {i} launched")
        await asyncio.sleep(0.01)

    stop = False
    while not stop:
        running_jobs, pending_jobs, _ = await _print_job_stats()
        stop = running_jobs == 0 and pending_jobs == 0
        await asyncio.sleep(1)

    print("All tasks done")


if __name__ == "__main__":
    asyncio.run(_run())
