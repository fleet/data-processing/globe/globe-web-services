import asyncio
import logging
from typing import Dict, List

from pygws.client.payload_interpreters import (
    DictOfFloatPayloadInterpreter,
    LogPayloadInterpreter,
)
from pygws.client.service_launcher import ServiceLauncher
from pygws.data_model.service_data import GwsArgument, GwsService
from pygws.playground.commons import run_launcher

logger = logging.getLogger("DictOfFloatServiceLauncher")


class SpatialResolutionInterpreter(DictOfFloatPayloadInterpreter):
    def process_dict(self, values: Dict[str, float]) -> None:
        logger.debug("Spatial resolution evaluated :")
        logger.debug(values)


class DictOfFloatServiceLauncher(ServiceLauncher):
    """
    Launch a service returning a map of float
    """

    def __init__(self):
        """
        Designate a service that returns a dict of float
        """
        super().__init__(
            "Evaluates spatial resolution of sounding files",
            payload_interpreters=[LogPayloadInterpreter(), SpatialResolutionInterpreter()],
        )

    def init_arguments(self, service: GwsService) -> List[GwsArgument]:
        """
        Set up the arguments
        """
        arguments = []
        # Search the argument by its type (infile) to create a GwsArgument
        for arg_conf in service.description.parameters:
            if arg_conf.type.startswith("infile"):
                arguments.append(GwsArgument(name=arg_conf.name, value=[r"d:/temp/sample.xsf.nc"]))
        return arguments


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    asyncio.run(run_launcher(DictOfFloatServiceLauncher()))
