import logging
import sys
from multiprocessing import Queue

import pytest

import pygws.data_model.flatbuffers.protocol_log_generated as proto_log
import pygws.data_model.rsocket_payload as r_payload
from pygws.client.payload_interpreters import LogPayloadInterpreter
from pygws.client.tcpsocket_client import GwsTcpSocketClient
from pygws.test.client.test_websocket_client import make_log_record

TCP_PORT = 38222


# Interpreter used by tcp server to receive logs emitted by the tcp client
class CatchingLogInterpreter(LogPayloadInterpreter):
    def __init__(self, log_queue: Queue):
        self.log_queue = log_queue

    def process_log(self, level: int, date: str, message: str, stack: str) -> None:
        # Received logs are store a the queue
        self.log_queue.put_nowait((level, date, message, stack))


@pytest.mark.asyncio
async def test_tcp_LogPayloadInterpreter(run_rsocket_server_on_tcpsocket):
    """
    Test TCP communication
    Logs are sent by a tcp client (like pyat service)
    to a tcp server (GWS server)
    """
    log_queue = Queue()  # Queue of logs, filled by the server

    # set up a rsocket server
    log_catcher = CatchingLogInterpreter(log_queue)
    rsocket_process = run_rsocket_server_on_tcpsocket(TCP_PORT, log_catcher)

    async def _send_logs(client, *args):
        try:
            raise ValueError("Raised for test")
        except:
            await client.fire_and_forget(
                r_payload.make_log_payload(make_log_record(logging.ERROR, exc_info=sys.exc_info()))
            )
        await client.fire_and_forget(r_payload.make_log_payload(make_log_record(logging.WARNING)))
        await client.fire_and_forget(r_payload.make_log_payload(make_log_record(logging.INFO)))
        await client.fire_and_forget(r_payload.make_log_payload(make_log_record(logging.DEBUG)))

    # Launch the client
    client = GwsTcpSocketClient("localhost", TCP_PORT, _send_logs)
    await client.run([])

    _log = log_queue.get(block=True, timeout=1.0)
    assert _log[0] == proto_log.LogLevel.ERROR
    assert len(_log[1]) == 19  # Iso format
    assert _log[2] == "One ERROR payload"
    assert "Traceback" in _log[3]

    _log = log_queue.get(block=True, timeout=1.0)
    assert _log[0] == proto_log.LogLevel.WARN
    assert len(_log[1]) == 19  # Iso format
    assert _log[2] == "One WARNING payload"
    assert len(_log[3]) == 0

    _log = log_queue.get(block=True, timeout=1.0)
    assert _log[0] == proto_log.LogLevel.INFO
    assert len(_log[1]) == 19  # Iso format
    assert _log[2] == "One INFO payload"
    assert len(_log[3]) == 0

    _log = log_queue.get(block=True, timeout=1.0)
    assert _log[0] == proto_log.LogLevel.DEBUG
    assert len(_log[1]) == 19  # Iso format
    assert _log[2] == "One DEBUG payload"
    assert len(_log[3]) == 0

    # Wait for server end
    rsocket_process.join(timeout=5)
    rsocket_process.kill()


if __name__ == "__main__":
    pytest.main(["-s", "-k", "test_tcp_LogPayloadInterpreter"])
