import logging
import sys
from pathlib import Path
from typing import Dict, List, Tuple

import pytest
from rsocket.rsocket_server import RSocketServer

import pygws.data_model.flatbuffers.protocol_log_generated as proto_log
import pygws.data_model.rsocket_payload as r_payload
from pygws.client.gws_error import GwsError
from pygws.client.payload_interpreters import (
    DictOfFloatPayloadInterpreter,
    FileListPayloadInterpreter,
    LogPayloadInterpreter,
    ProgressPayloadInterpreter,
)
from pygws.client.websocket_client import GwsWebSocketClient


@pytest.mark.asyncio
async def test_no_rsocket_server():
    with pytest.raises(GwsError) as excinfo:
        client = GwsWebSocketClient("localhost", 37999)
        await client.run([])
    assert "Unable to connect to GWS" in str(excinfo.value)


async def _send_map_of_double(rsocket_server: RSocketServer):
    """
    Server will send map of float to client
    """
    await rsocket_server.fire_and_forget(
        r_payload.make_map_of_float_payload(
            {
                "meter": 11.8,
                "degree": 0.0051,
            }
        )
    )
    await rsocket_server.request_response(r_payload.make_terminate_payload())


@pytest.mark.asyncio
async def test_DictOfFloatPayloadInterpreter(run_rsocket_server_on_websocket):
    # run_rsocket_server_on_websocket invoke by pytest to set up a rsocket server
    rsocket_process = run_rsocket_server_on_websocket(37999, _send_map_of_double)

    # Interpreter used by client to receive the floats emitted by the server
    floatInterpreter = DictOfFloatPayloadInterpreter()

    # Set up a client as if a job was submitted to GWS
    async def _run_client():
        """
        Coroutine to set up an run a client as if a job was submitted to GWS
        """
        client = GwsWebSocketClient("localhost", 37999)
        await client.run([floatInterpreter])

        # And data received
        assert floatInterpreter.intercepted_floats["meter"] == 11.8
        assert floatInterpreter.intercepted_floats["degree"] == 0.0051

    await _run_client()
    # Wait for server end
    rsocket_process.join(timeout=5)
    rsocket_process.kill()


def make_log_record(level: int, exc_info=None):
    return logging.LogRecord(
        name="Logger name",
        level=level,
        pathname="Calling function",
        lineno=12,
        msg=f"One {logging.getLevelName(level)} payload",
        args=None,
        exc_info=exc_info,
    )


async def _send_logs(rsocket_server: RSocketServer):
    """
    Server will send logging messages to client
    """
    try:
        raise ValueError("Raised for test")
    except:
        await rsocket_server.fire_and_forget(
            r_payload.make_log_payload(make_log_record(logging.ERROR, exc_info=sys.exc_info()))
        )
    await rsocket_server.fire_and_forget(r_payload.make_log_payload(make_log_record(logging.WARNING)))
    await rsocket_server.fire_and_forget(r_payload.make_log_payload(make_log_record(logging.INFO)))
    await rsocket_server.fire_and_forget(r_payload.make_log_payload(make_log_record(logging.DEBUG)))
    await rsocket_server.request_response(r_payload.make_terminate_payload())


@pytest.mark.asyncio
async def test_LogPayloadInterpreter(run_rsocket_server_on_websocket):
    # run_rsocket_server_on_websocket invoke by pytest to set up a rsocket server
    rsocket_process = run_rsocket_server_on_websocket(37999, _send_logs)

    # Interpreter used by client to receive logs emitted by the server
    class CatchingLogInterpreter(LogPayloadInterpreter):
        received_logs: Dict[int, Tuple] = {}

        def process_log(self, level: int, date: str, message: str, stack: str) -> None:
            self.received_logs[level] = (
                date,
                message,
                stack,
            )

    logInterpreter = CatchingLogInterpreter()

    # Set up a client as if a job was submitted to GWS
    async def _run_client():
        """
        Coroutine to set up an run a client as if a job was submitted to GWS
        """
        client = GwsWebSocketClient("localhost", 37999)
        await client.run([logInterpreter])

        # And data received
        assert len(logInterpreter.received_logs) == 4

        _log = logInterpreter.received_logs[proto_log.LogLevel.ERROR]
        assert len(_log[0]) == 19  # Iso format
        assert _log[1] == "One ERROR payload"
        assert "Traceback" in _log[2]

        _log = logInterpreter.received_logs[proto_log.LogLevel.WARN]
        assert len(_log[0]) == 19  # Iso format
        assert _log[1] == "One WARNING payload"
        assert len(_log[2]) == 0

        _log = logInterpreter.received_logs[proto_log.LogLevel.INFO]
        assert len(_log[0]) == 19  # Iso format
        assert _log[1] == "One INFO payload"
        assert len(_log[2]) == 0

        _log = logInterpreter.received_logs[proto_log.LogLevel.DEBUG]
        assert len(_log[0]) == 19  # Iso format
        assert _log[1] == "One DEBUG payload"
        assert len(_log[2]) == 0

    await _run_client()
    # Wait for server end
    rsocket_process.join(timeout=5)
    rsocket_process.kill()


async def _send_files(rsocket_server: RSocketServer):
    """
    Server will send some file paths to client
    """
    await rsocket_server.fire_and_forget(r_payload.make_files_payload(["~/file1.xsf.nc", "~/file2.xsf.nc"]))
    await rsocket_server.request_response(r_payload.make_terminate_payload())


@pytest.mark.asyncio
async def test_FileListPayloadInterpreter(run_rsocket_server_on_websocket):
    # run_rsocket_server_on_websocket invoke by pytest to set up a rsocket server
    rsocket_process = run_rsocket_server_on_websocket(37999, _send_files)

    fileListPayloadInterpreter = FileListPayloadInterpreter()

    # Set up a client as if a job was submitted to GWS
    async def _run_client():
        """
        Coroutine to set up an run a client as if a job was submitted to GWS
        """
        client = GwsWebSocketClient("localhost", 37999)
        await client.run([fileListPayloadInterpreter])

        # And data received
        assert len(fileListPayloadInterpreter.intercepted_files) == 2
        assert fileListPayloadInterpreter.intercepted_files[0] == Path("~/file1.xsf.nc")
        assert fileListPayloadInterpreter.intercepted_files[1] == Path("~/file2.xsf.nc")

    await _run_client()
    # Wait for server end
    rsocket_process.join(timeout=5)
    rsocket_process.kill()


async def _send_progression(rsocket_server: RSocketServer):
    """
    Server will send progression to client
    """
    for progress in range(100):
        await rsocket_server.fire_and_forget(r_payload.make_progress_payload("the task in progress", progress / 100.0))
    await rsocket_server.request_response(r_payload.make_terminate_payload())


@pytest.mark.asyncio
async def test_ProgressPayloadInterpreter(run_rsocket_server_on_websocket):
    # run_rsocket_server_on_websocket invoke by pytest to set up a rsocket server
    rsocket_process = run_rsocket_server_on_websocket(37999, _send_progression)

    # Interpreter used by client to receive logs emitted by the server
    class CatchingProgressPayloadInterpreter(ProgressPayloadInterpreter):
        task_name: str = ""
        received_progress: List[float] = []

        def process_progress(self, task_name: str, progess: float) -> None:
            self.task_name = task_name
            self.received_progress.append(progess)

    progressPayloadInterpreter = CatchingProgressPayloadInterpreter()

    # Set up a client as if a job was submitted to GWS
    async def _run_client():
        """
        Coroutine to set up an run a client as if a job was submitted to GWS
        """
        client = GwsWebSocketClient("localhost", 37999)
        await client.run([progressPayloadInterpreter])

        # And data received
        assert progressPayloadInterpreter.task_name == "the task in progress"
        assert len(progressPayloadInterpreter.received_progress) == 100
        for progress in range(100):
            assert pytest.approx(progressPayloadInterpreter.received_progress[progress]) == progress / 100.0

    await _run_client()
    # Wait for server end
    rsocket_process.join(timeout=5)
    rsocket_process.kill()


if __name__ == "__main__":
    pytest.main(["-s", "-k", "test_no_rsocket_server"])
