import pytest
from pytest_httpx import HTTPXMock

import pygws.client.gws_server_proxy as gws_server
from pygws.client.gws_error import GwsError
from pygws.data_model.service_data import GwsArgument


@pytest.mark.asyncio
async def test_no_gws():
    with pytest.raises(GwsError) as excinfo:
        await gws_server.get_all_services()
    assert "GWS not reachable" in str(excinfo.value)


@pytest.mark.asyncio
async def test_get_all_services(mock_all_services):
    # Set mock
    mock_all_services(7777)

    services = await gws_server.get_all_services()
    assert len(services) == 4

    # Search one specific service
    filtered_service = list(filter(lambda s: s.name == "Evaluates geobox of sounding files", services))
    assert len(filtered_service) == 1
    assert filtered_service[0].id == 6


@pytest.mark.asyncio
async def test_get_service_by_id(mock_service):
    # Set mock
    mock_service(6, 7777)

    gws_service = await gws_server.get_service(6)
    assert gws_service.id == 6
    assert gws_service.description.name == "Evaluates geobox of sounding files"
    assert len(gws_service.description.parameters) == 1
    assert gws_service.description.parameters[0].name == "i_paths"


@pytest.mark.asyncio
async def test_get_service_by_name(mock_all_services, mock_service):
    # Set mock
    mock_all_services(7777)
    mock_service(6, 7777)

    gws_service = await gws_server.get_service_by_name("Evaluates geobox of sounding files")
    assert gws_service.id == 6
    assert gws_service.description.name == "Evaluates geobox of sounding files"
    assert len(gws_service.description.parameters) == 1
    assert gws_service.description.parameters[0].name == "i_paths"


@pytest.mark.asyncio
async def test_get_jobs(mock_all_jobs):
    # Set mock
    mock_all_jobs(7777)

    jobs = await gws_server.get_all_jobs()
    assert len(jobs) == 2

    # Search one specific job
    filtered_job = list(filter(lambda j: j.service_id == 95, jobs))
    assert len(filtered_job) == 1
    assert filtered_job[0].service_name == "Convert sounding file to XSF"
    assert filtered_job[0].websocket_port == 62318
    assert filtered_job[0].status == "SUCCESS"


@pytest.mark.asyncio
async def test_get_job_2(mock_job):
    # Set mock
    mock_job(2, 95, 62318, 7777)

    job_2 = await gws_server.get_job(2)

    assert job_2.service_name == "Convert sounding file to XSF"
    assert job_2.websocket_port == 62318
    assert job_2.status == "SUCCESS"


@pytest.mark.asyncio
async def test_launch_service(httpx_mock: HTTPXMock):
    new_job = {
        "job_id": 9,
        "service_id": 6,
    }

    httpx_mock.add_response(
        url="http://localhost:7777/gws/services/6?logbook=false",
        method="POST",
        match_json={"i_paths": "file.all"},  # Expected content, or test failed
        json=new_job,  # Response
    )

    await gws_server.launch_service(service_id=6, arguments=[GwsArgument(name="i_paths", value="file.all")])


if __name__ == "__main__":
    pytest.main(["-s", "-k", "test_no_gws"])
