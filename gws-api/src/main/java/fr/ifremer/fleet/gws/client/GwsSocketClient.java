package fr.ifremer.fleet.gws.client;

import fr.ifremer.fleet.gws.rsocket.GwsPayload;
import io.rsocket.Payload;
import io.rsocket.RSocket;
import io.rsocket.core.RSocketClient;
import io.rsocket.core.RSocketConnector;
import io.rsocket.transport.netty.client.WebsocketClientTransport;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;
import reactor.core.publisher.Sinks;
import reactor.core.publisher.Sinks.Many;
import reactor.core.publisher.Sinks.One;
import reactor.netty.http.client.HttpClient;

/**
 * Communication channel with the service via an RSocket client.
 *
 *
 * <pre>
 * In RSocket architecture, there are 4 RSockets involved in socket communication.
 * Here is represented the both RSocket of the client-side (Globe).
 * The same RSocket exists on the server-side (GWS).
 *
 *                                                         +------------+                        +------------+
 *        ({@link #responderSocket})  <------Payload------ |            |  <------Payload------  |            |
 *                                                         |  Websocket |                        |    GWS     |
 *         ({@link #emitterSocket})   ------Payload------> |            |  ------Payload------>  |            |
 *                                                         +------------+                        +------------+
 * </pre>
 */
@Slf4j
public class GwsSocketClient {

    /**
     * RSocket used to obtain connections from the websocket as needed
     */
    private final RSocketClient connectionSocket;
    /**
     * Emitter part of the RSocket connection. Used to emit a request to GWS
     */
    private final One<RSocket> emitterSocket = Sinks.one();
    /**
     * Responder part of the RSocket connection when a request is received
     */
    private RSocket responderSocket;

    /**
     * Constructor
     *
     * @param queueOfReceivedPayload queue where payloads received from websocket are pushed
     */
    public GwsSocketClient(String websocketHost, int websocketPort, Many<GwsPayload> queueOfReceivedPayload) {
        HttpClient httpClient = HttpClient.create()//
                .host(websocketHost)//
                .port(websocketPort);
        Mono<RSocket> source = RSocketConnector.create()//
                .acceptor((setup, sendingSocket) -> {
                    // Reacts to the closure of the server socket
                    this.emitterSocket.tryEmitValue(sendingSocket);
                    sendingSocket.onClose().doFinally(s -> {
                        queueOfReceivedPayload.tryEmitComplete();
                        dispose();
                    }).onErrorComplete(e -> {
                        log.info("{} received. Rsocket connection lost (port {})", e.getClass().getSimpleName(),
                                websocketPort);
                        queueOfReceivedPayload.tryEmitComplete();
                        dispose();
                        return true;
                    }).subscribe();

                    // Reacts to the reception of a payload and responds to the server
                    this.responderSocket = new RSocket() {
                        @Override
                        public Mono<Void> fireAndForget(Payload payload) {
                            GwsPayload gwsPayload = GwsPayload.from(payload);
                            if (!gwsPayload.route().isEmpty()) {
                                if (queueOfReceivedPayload.tryEmitNext(gwsPayload).isFailure())
                                    log.warn("Failed to process payload with route {}. Ignored", gwsPayload.route());
                            } else
                                log.info("Data received without route information. Ignored");
                            return Mono.empty();
                        }
                    };
                    return Mono.just(responderSocket);
                })

                .connect(WebsocketClientTransport.create(httpClient, "/"));

        connectionSocket = RSocketClient.from(source);
    }

    /**
     * Connect to the remote RSocket end-point
     */
    public void connect() {
        connectionSocket.connect();
    }

    /**
     * Fire a payload
     */
    public Mono<Void> fireAndForgetPayload(GwsPayload payload) {
        String route = payload.route();
        log.info("Send a payload to '{}' route", route);
        return emitterSocket.asMono().flatMap(socket -> socket.fireAndForget(payload.makePayload()));
    }

    /**
     * Request response for a payload
     */
    public Mono<Payload> requestResponseForPayload(GwsPayload payload) {
        String route = payload.route();
        log.info("Request response for '{}' route", route);
        return emitterSocket.asMono().flatMap(socket -> socket.requestResponse(payload.makePayload()));
    }

    /**
     * Close the connection
     */
    public void dispose() {
        // Dispose all RSockets to allow Websocket closure
        if (responderSocket != null && !responderSocket.isDisposed())
            responderSocket.dispose();
        emitterSocket.asMono().subscribe(socket -> socket.dispose());
        if (connectionSocket != null && !connectionSocket.isDisposed())
            connectionSocket.dispose();
    }

}
