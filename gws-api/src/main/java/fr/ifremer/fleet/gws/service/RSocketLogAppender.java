package fr.ifremer.fleet.gws.service;

import com.google.flatbuffers.FlatBufferBuilder;
import fr.ifremer.fleet.gws.rsocket.GwsPayload;
import fr.ifremer.fleet.gws.rsocket.flatbuffers.LogLevel;
import fr.ifremer.fleet.gws.rsocket.flatbuffers.LogPayload;
import io.rsocket.RSocket;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.appender.AbstractAppender;
import org.apache.logging.log4j.core.config.Property;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * Log4j appender to redirect logs to RSocket
 */
public class RSocketLogAppender extends AbstractAppender {
    /**
     * Open connection
     */
    private final RSocket connectionSocket;

    /**
     * Constructor
     */
    public RSocketLogAppender(String name, RSocket connectionSocket) {
        super(name, null, null, true, Property.EMPTY_ARRAY);
        this.connectionSocket = connectionSocket;
    }

    /**
     * Intercept a log event. Translate to a LogPayload
     */
    @Override
    public void append(LogEvent event) {
        if (!connectionSocket.isDisposed()) {
            int level = switch (event.getLevel().getStandardLevel()) {
                case TRACE, DEBUG -> LogLevel.DEBUG;
                case INFO -> LogLevel.INFO;
                case WARN -> LogLevel.WARN;
                default -> LogLevel.ERROR;
            };
            FlatBufferBuilder builder = new FlatBufferBuilder();
            int msgOffset = builder.createString(event.getMessage().getFormattedMessage());
            int dateOffset = builder.createString(event.getInstant().toString());
            String stack = "";
            if (level >= LogLevel.WARN && event.getThrown() != null) {
                StringWriter sw = new StringWriter();
                PrintWriter pw = new PrintWriter(sw);
                event.getThrown().printStackTrace(pw);
                stack = sw.toString();
            }
            int stackOffset = builder.createString(stack);
            builder.finish(LogPayload.createLogPayload(builder, level, msgOffset, dateOffset, stackOffset));
            GwsPayload payload = new GwsPayload(LogPayload.class.getSimpleName(), builder.dataBuffer());
            connectionSocket.fireAndForget(payload.makePayload()).onErrorComplete().block();
        }
    }
}
