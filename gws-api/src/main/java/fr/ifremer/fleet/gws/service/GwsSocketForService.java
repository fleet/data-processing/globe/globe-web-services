package fr.ifremer.fleet.gws.service;

import com.google.flatbuffers.FlatBufferBuilder;
import fr.ifremer.fleet.gws.rsocket.GwsPayload;
import fr.ifremer.fleet.gws.rsocket.flatbuffers.ProgressPayload;
import io.rsocket.Payload;
import io.rsocket.RSocket;
import io.rsocket.SocketAcceptor;
import io.rsocket.core.RSocketConnector;
import io.rsocket.transport.netty.client.TcpClientTransport;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.config.Configuration;
import org.apache.logging.log4j.core.filter.MarkerFilter;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;
import reactor.core.publisher.Mono;

import java.util.function.Consumer;

/**
 * Communication channel used by a service to send payload to the client via a RSocket.
 */
@Slf4j
public class GwsSocketForService {

    /**
     * Opened connection
     */
    private final RSocket connectionSocket;

    /**
     * Logger appender living during the service execution
     */
    private final RSocketLogAppender logAppender;


    /**
     * Logger appender living during the service execution
     */
    private final Marker logMarker;


    /**
     * Constructor
     */
    public GwsSocketForService(String socketHost, int socketPort, Consumer<GwsPayload> payloadConsumer) {
        log.info("Connecting to socket {}:{}", socketHost, socketPort);
        TcpClientTransport transport = TcpClientTransport.create(socketHost, socketPort);
        this.connectionSocket = RSocketConnector
                .create()
                .acceptor(
                        SocketAcceptor.forFireAndForget(
                                p -> {
                                    // Redirect client message to service
                                    payloadConsumer.accept(GwsPayload.from(p));
                                    p.release();
                                    return Mono.empty();
                                }))
                .connect(transport)
                .block();

        // Add an Appender Log4j to redirect all logs
        LoggerContext context = LoggerContext.getContext(false);
        Configuration config = context.getConfiguration();
        Logger logger = context.getLogger(GwsSocketForService.class.getName() + "_" + socketPort);
        logMarker = MarkerFactory.getMarker(socketHost + ":" + socketPort);
        logAppender = new RSocketLogAppender(logMarker.getName(), this.connectionSocket);
        logAppender.addFilter(MarkerFilter.createFilter(logMarker.getName(), null, null));
        logAppender.start();
        config.getRootLogger().addAppender(logAppender, null, null);

        log.info(logMarker, "Connected to socket {}:{}", socketHost, socketPort);
    }

    /**
     * Fire a payload
     */
    public Mono<Void> fireAndForgetPayload(GwsPayload payload) {
        String route = payload.route();
        log.info("Send a payload to '{}' route", route);
        return connectionSocket.fireAndForget(payload.makePayload());
    }

    /**
     * Request response for a payload
     */
    public Mono<Payload> requestResponseForPayload(GwsPayload payload) {
        String route = payload.route();
        log.info("Request response for '{}' route", route);
        return connectionSocket.requestResponse(payload.makePayload());
    }

    /**
     * Send a ProgressPayload to the client
     */
    public void sendProgression(float progress, String taskname) {
        FlatBufferBuilder builder = new FlatBufferBuilder();
        int tasknameOffset = builder.createString(taskname);
        builder.finish(ProgressPayload.createProgressPayload(builder, progress, tasknameOffset));
        GwsPayload payload = new GwsPayload(ProgressPayload.class.getSimpleName(), builder.dataBuffer());
        connectionSocket.fireAndForget(payload.makePayload()).onErrorComplete().block();
    }

    /**
     * Close the connection
     */
    public void dispose() {
        log.info(logMarker, "Service finished. Closing connections...");
        connectionSocket.fireAndForget(GwsPayload.makeTerminatePayload().makePayload()).onErrorComplete().block();

        LoggerContext context = LoggerContext.getContext(false);
        Configuration config = context.getConfiguration();
        logAppender.stop();
        config.getRootLogger().removeAppender(this.logAppender.getName());

        if (!connectionSocket.isDisposed()) {
            connectionSocket.dispose();
        }
    }

    public Marker getLogMarker() {
        return logMarker;
    }

}
