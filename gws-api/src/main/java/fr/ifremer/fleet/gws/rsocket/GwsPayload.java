package fr.ifremer.fleet.gws.rsocket;

import com.google.flatbuffers.FlatBufferBuilder;
import fr.ifremer.fleet.gws.rsocket.flatbuffers.LogPayload;
import io.netty.buffer.*;
import io.rsocket.Payload;
import io.rsocket.metadata.*;
import io.rsocket.util.DefaultPayload;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.List;

/**
 * Message received from GWS.
 */
public record GwsPayload(
        /** Routing information of the payload */
        String route, /** Content of the message. Null means no message */
        ByteBuffer payload) {

    /**
     * Common route of payloads
     */
    public static final String LOG_ROUTE = "LogPayload";
    public static final String TERMINATE_ROUTE = "terminate";
    public static final String CANCEL_ROUTE = "cancel";
    public static final String RUN_SERVICE = "run_service";

    /**
     * Creates a payload with a single string as message
     */
    public GwsPayload(String route, String message) {
        this(route, ByteBuffer.wrap(message.getBytes(StandardCharsets.UTF_8)));
    }

    /**
     * Creates a payload with a no message
     */
    public GwsPayload(String route) {
        this(route, (ByteBuffer) null);
    }

    /**
     * Convert to a RSocket Payload
     */
    public Payload makePayload() {
        ByteBuf routingMetadata = TaggingMetadataCodec.createTaggingContent(ByteBufAllocator.DEFAULT, List.of(route));
        CompositeByteBuf metadata = ByteBufAllocator.DEFAULT.compositeBuffer();
        CompositeMetadataCodec.encodeAndAddMetadata(metadata, ByteBufAllocator.DEFAULT,
                WellKnownMimeType.MESSAGE_RSOCKET_ROUTING, routingMetadata);

        ByteBuf message = payload == null ? new EmptyByteBuf(ByteBufAllocator.DEFAULT)
                : Unpooled.wrappedBuffer(payload);
        return DefaultPayload.create(message, metadata);
    }

    /**
     * Build a GwsPayload from Payload by extracting route and data
     */
    public static GwsPayload from(Payload payload) {
        String metadataName = WellKnownMimeType.MESSAGE_RSOCKET_ROUTING.getString();

        ByteBuf metadata = payload.sliceMetadata();
        CompositeMetadata compositeMetadata = new CompositeMetadata(metadata, false);

        for (CompositeMetadata.Entry entry : compositeMetadata) {
            if (metadataName.equals(entry.getMimeType())) {
                return new GwsPayload(new RoutingMetadata(entry.getContent()).iterator().next(), payload.getData());
            }
        }
        return new GwsPayload("", payload.getData());
    }

    /**
     * Creates an instance of GwsPayload to transport a log message
     */
    public static GwsPayload makeLogPayload(int logLevel, String message, String date, String stack) {
        FlatBufferBuilder bufferBuilder = new FlatBufferBuilder();
        int msgOffset = bufferBuilder.createString(message);
        int dateOffset = bufferBuilder.createString(date);
        int stackOffset = bufferBuilder.createString(stack);
        int msgTable = LogPayload.createLogPayload(bufferBuilder, logLevel, msgOffset, dateOffset, stackOffset);
        bufferBuilder.finish(msgTable);
        return new GwsPayload(LOG_ROUTE, bufferBuilder.dataBuffer());
    }

    /**
     * Creates an instance of GwsPayload to send a terminate signal
     */
    public static GwsPayload makeTerminatePayload() {
        return new GwsPayload(TERMINATE_ROUTE);
    }

    /**
     * Creates an instance of GwsPayload to send a cancel signal
     */
    public static GwsPayload makeCancelPayload() {
        return new GwsPayload(CANCEL_ROUTE);
    }

}