// automatically generated by the FlatBuffers compiler, do not modify

package fr.ifremer.fleet.gws.rsocket.flatbuffers;

import com.google.flatbuffers.BaseVector;
import com.google.flatbuffers.BooleanVector;
import com.google.flatbuffers.ByteVector;
import com.google.flatbuffers.Constants;
import com.google.flatbuffers.DoubleVector;
import com.google.flatbuffers.FlatBufferBuilder;
import com.google.flatbuffers.FloatVector;
import com.google.flatbuffers.IntVector;
import com.google.flatbuffers.LongVector;
import com.google.flatbuffers.ShortVector;
import com.google.flatbuffers.StringVector;
import com.google.flatbuffers.Struct;
import com.google.flatbuffers.Table;
import com.google.flatbuffers.UnionVector;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

@SuppressWarnings("unused")
public final class ProgressPayload extends Table {
  public static void ValidateVersion() { Constants.FLATBUFFERS_23_5_26(); }
  public static ProgressPayload getRootAsProgressPayload(ByteBuffer _bb) { return getRootAsProgressPayload(_bb, new ProgressPayload()); }
  public static ProgressPayload getRootAsProgressPayload(ByteBuffer _bb, ProgressPayload obj) { _bb.order(ByteOrder.LITTLE_ENDIAN); return (obj.__assign(_bb.getInt(_bb.position()) + _bb.position(), _bb)); }
  public void __init(int _i, ByteBuffer _bb) { __reset(_i, _bb); }
  public ProgressPayload __assign(int _i, ByteBuffer _bb) { __init(_i, _bb); return this; }

  public float progress() { int o = __offset(4); return o != 0 ? bb.getFloat(o + bb_pos) : 0.0f; }
  public String taskname() { int o = __offset(6); return o != 0 ? __string(o + bb_pos) : null; }
  public ByteBuffer tasknameAsByteBuffer() { return __vector_as_bytebuffer(6, 1); }
  public ByteBuffer tasknameInByteBuffer(ByteBuffer _bb) { return __vector_in_bytebuffer(_bb, 6, 1); }

  public static int createProgressPayload(FlatBufferBuilder builder,
      float progress,
      int tasknameOffset) {
    builder.startTable(2);
    ProgressPayload.addTaskname(builder, tasknameOffset);
    ProgressPayload.addProgress(builder, progress);
    return ProgressPayload.endProgressPayload(builder);
  }

  public static void startProgressPayload(FlatBufferBuilder builder) { builder.startTable(2); }
  public static void addProgress(FlatBufferBuilder builder, float progress) { builder.addFloat(0, progress, 0.0f); }
  public static void addTaskname(FlatBufferBuilder builder, int tasknameOffset) { builder.addOffset(1, tasknameOffset, 0); }
  public static int endProgressPayload(FlatBufferBuilder builder) {
    int o = builder.endTable();
    return o;
  }
  public static void finishProgressPayloadBuffer(FlatBufferBuilder builder, int offset) { builder.finish(offset); }
  public static void finishSizePrefixedProgressPayloadBuffer(FlatBufferBuilder builder, int offset) { builder.finishSizePrefixed(offset); }

  public static final class Vector extends BaseVector {
    public Vector __assign(int _vector, int _element_size, ByteBuffer _bb) { __reset(_vector, _element_size, _bb); return this; }

    public ProgressPayload get(int j) { return get(new ProgressPayload(), j); }
    public ProgressPayload get(ProgressPayload obj, int j) {  return obj.__assign(__indirect(__element(j), bb), bb); }
  }
}

