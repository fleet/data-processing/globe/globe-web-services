ECHO "FLATC"
FOR %%G IN (%~dp0\*.fbs) DO (
    flatc.exe --java --java-package-prefix fr.ifremer.fleet.gws.rsocket.flatbuffers -o %~dp0\..\src\main\java %%G
    flatc.exe --python --gen-onefile -o %~dp0\..\python\pygws\data_model\flatbuffers %%G
    flatc.exe --ts -o %~dp0\..\typescript\src\flatbuffers %%G
)
