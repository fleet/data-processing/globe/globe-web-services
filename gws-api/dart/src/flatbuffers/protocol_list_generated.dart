// automatically generated by the FlatBuffers compiler, do not modify
// ignore_for_file: unused_import, unused_field, unused_element, unused_local_variable

import 'dart:typed_data' show Uint8List;
import 'package:flat_buffers/flat_buffers.dart' as fb;


class FileListPayload {
  FileListPayload._(this._bc, this._bcOffset);
  factory FileListPayload(List<int> bytes) {
    final rootRef = fb.BufferContext.fromBytes(bytes);
    return reader.read(rootRef, 0);
  }

  static const fb.Reader<FileListPayload> reader = _FileListPayloadReader();

  final fb.BufferContext _bc;
  final int _bcOffset;

  List<String>? get filePaths => const fb.ListReader<String>(fb.StringReader()).vTableGetNullable(_bc, _bcOffset, 4);

  @override
  String toString() {
    return 'FileListPayload{filePaths: ${filePaths}}';
  }
}

class _FileListPayloadReader extends fb.TableReader<FileListPayload> {
  const _FileListPayloadReader();

  @override
  FileListPayload createObject(fb.BufferContext bc, int offset) => 
    FileListPayload._(bc, offset);
}

class FileListPayloadBuilder {
  FileListPayloadBuilder(this.fbBuilder);

  final fb.Builder fbBuilder;

  void begin() {
    fbBuilder.startTable(1);
  }

  int addFilePathsOffset(int? offset) {
    fbBuilder.addOffset(0, offset);
    return fbBuilder.offset;
  }

  int finish() {
    return fbBuilder.endTable();
  }
}

class FileListPayloadObjectBuilder extends fb.ObjectBuilder {
  final List<String>? _filePaths;

  FileListPayloadObjectBuilder({
    List<String>? filePaths,
  })
      : _filePaths = filePaths;

  /// Finish building, and store into the [fbBuilder].
  @override
  int finish(fb.Builder fbBuilder) {
    final int? filePathsOffset = _filePaths == null ? null
        : fbBuilder.writeList(_filePaths!.map(fbBuilder.writeString).toList());
    fbBuilder.startTable(1);
    fbBuilder.addOffset(0, filePathsOffset);
    return fbBuilder.endTable();
  }

  /// Convenience method to serialize to byte list.
  @override
  Uint8List toBytes([String? fileIdentifier]) {
    final fbBuilder = fb.Builder(deduplicateTables: false);
    fbBuilder.finish(finish(fbBuilder), fileIdentifier);
    return fbBuilder.buffer;
  }
}
class StringListPayload {
  StringListPayload._(this._bc, this._bcOffset);
  factory StringListPayload(List<int> bytes) {
    final rootRef = fb.BufferContext.fromBytes(bytes);
    return reader.read(rootRef, 0);
  }

  static const fb.Reader<StringListPayload> reader = _StringListPayloadReader();

  final fb.BufferContext _bc;
  final int _bcOffset;

  List<String>? get strings => const fb.ListReader<String>(fb.StringReader()).vTableGetNullable(_bc, _bcOffset, 4);

  @override
  String toString() {
    return 'StringListPayload{strings: ${strings}}';
  }
}

class _StringListPayloadReader extends fb.TableReader<StringListPayload> {
  const _StringListPayloadReader();

  @override
  StringListPayload createObject(fb.BufferContext bc, int offset) => 
    StringListPayload._(bc, offset);
}

class StringListPayloadBuilder {
  StringListPayloadBuilder(this.fbBuilder);

  final fb.Builder fbBuilder;

  void begin() {
    fbBuilder.startTable(1);
  }

  int addStringsOffset(int? offset) {
    fbBuilder.addOffset(0, offset);
    return fbBuilder.offset;
  }

  int finish() {
    return fbBuilder.endTable();
  }
}

class StringListPayloadObjectBuilder extends fb.ObjectBuilder {
  final List<String>? _strings;

  StringListPayloadObjectBuilder({
    List<String>? strings,
  })
      : _strings = strings;

  /// Finish building, and store into the [fbBuilder].
  @override
  int finish(fb.Builder fbBuilder) {
    final int? stringsOffset = _strings == null ? null
        : fbBuilder.writeList(_strings!.map(fbBuilder.writeString).toList());
    fbBuilder.startTable(1);
    fbBuilder.addOffset(0, stringsOffset);
    return fbBuilder.endTable();
  }

  /// Convenience method to serialize to byte list.
  @override
  Uint8List toBytes([String? fileIdentifier]) {
    final fbBuilder = fb.Builder(deduplicateTables: false);
    fbBuilder.finish(finish(fbBuilder), fileIdentifier);
    return fbBuilder.buffer;
  }
}
